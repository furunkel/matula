package matula.io;

import matula.lang.Node;
import matula.runtime.Runtime;

public class NoData extends Node
{
  public static final int CLASS_ID = Runtime.requestClassId("matula.io.NoData");
  
  public NoData()
  {
    super(CLASS_ID);  
  }
  
}

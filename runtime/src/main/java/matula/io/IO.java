package matula.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Scanner;

import matula.lang.Node;
import matula.lang.LinearNode;
import matula.lang.Integer;
import matula.lang.String;
import matula.runtime.NoMatchException;
import matula.runtime.Runtime;


public final class IO extends LinearNode
{
  private static final class EmptyOutputStream extends OutputStream
  {
    public void write(int b){}
  }

  private static final class EmptyInputStream extends InputStream
  {
    public int read(){return -1;}
  }

  public static final int CLASS_ID = Runtime.requestClassId("matula.io.IO");
  
  private final InputStream inputStream;
  private final OutputStream outputStream;

  protected IO(InputStream inputStream, OutputStream outputStream)
  {
    super(CLASS_ID);

    this.inputStream = inputStream;
    this.outputStream = outputStream;
  }

  protected IO(InputStream inputStream)
  {
    this(inputStream, EMPTY_OUTPUT_STREAM);
  }

  protected IO(OutputStream outputStream)
  {
    this(EMPTY_INPUT_STREAM, outputStream);
  }

  protected IO(IO io)
  {
    this(io.inputStream, io.outputStream);
  }
 
  public static final int LENGTH = 0;
  public static final int OFFSET = 1;
  public static final int THIS   = 2;
  public static final int TO     = 3;
  public static final int FROM   = 4;
  public static final int DATA   = 5;
  
  private static final InputStream EMPTY_INPUT_STREAM = new EmptyInputStream();
  private static final OutputStream EMPTY_OUTPUT_STREAM = new EmptyOutputStream();

  private static final Node STDIN = new IO(System.in);
  private static final Node STDOUT = new IO(System.out);

  private static final Result STDIN_RESULT = new Result(new NoData(), STDIN);
  private static final Result STDOUT_RESULT = new Result(new NoData(), STDOUT);
  

  public static Node stdin()
  {
    return STDIN_RESULT;
  }

  public static Node stdout()
  {
    return STDOUT_RESULT;
  }

  public static Node write(int arg0, Node length, int arg1, Node offset, int arg2, Node _this, int arg3, Node to)
  {
    if(!(arg0      == LENGTH &&
         arg1      == OFFSET &&
         arg2      == DATA &&
         arg3      == TO &&
         to.id     == Result.CLASS_ID &&
         length.id == Integer.CLASS_ID &&
         offset.id == Integer.CLASS_ID))
    {
      throw new NoMatchException();
    }

    Result result = (Result) to;

    IO io = (IO) result.io;

    Node dataNode = ((Result)_this).data;
    
    int len = ((Integer)length).value;
    int off = ((Integer)offset).value;

    java.lang.String data = dataNode.asString().toString();
    byte[] buffer = data.getBytes();

    try {
      io.outputStream.write(buffer, off, len);
      return new String(buffer);
    } catch (java.io.IOException ex) {
      return new matula.io.IOException(ex); 
    }
    
  }
  
  public static Node read(int arg0, Node from, int arg1, Node length, int arg2, Node offset)
  {
    if(!(arg0      == FROM && 
         arg1      == LENGTH &&
         arg2      == OFFSET &&
         from.id   == Result.CLASS_ID &&
         length.id == Integer.CLASS_ID &&
         offset.id == Integer.CLASS_ID))
    {
      throw new NoMatchException();
    }

    Result result = (Result) from;
    IO io = (IO) result.io;
    io.reference();
   
    int len = ((Integer)length).value;
    int off = ((Integer)offset).value;

    byte[] buffer = new byte[len];

    try {
      io.inputStream.read(buffer, off, len);
    } catch (java.io.IOException ex) {
      return new matula.io.IOException(ex); 
    }
    return new Result(new String(buffer), io);
  }

  public static Node printLine_data_to(Node data, Node to)
  {
    if(!(to.id == Result.CLASS_ID))
    {
      throw new NoMatchException();
    }

    Result result = (Result) to;
    IO io = (IO) result.io;

    io.reference();
    
    Node dataNode = data;
    
    java.lang.String stringWithNewline = dataNode.asString().toString() + "\n";

    try {
      io.outputStream.write(stringWithNewline.getBytes());
    }
    catch(java.io.IOException e)
    {
      return new matula.io.IOException(e);
    }

    return new Result(new IO(io)); 
  }

  public static Node readLine(int arg0, Node from)
  {
    if(!(arg0     == FROM &&
         from.id  == Result.CLASS_ID))
    {
      throw new NoMatchException();  
    }

    Result result = (Result) from;
    IO io = (IO) result.io;
   
    io.reference();

    Scanner scanner = new Scanner(io.inputStream);
    
    java.lang.String line = scanner.nextLine();
    Node data = new String(line);

    return new Result(data, io);
  }
}

package matula.io;

import matula.lang.Node;
import matula.runtime.Runtime;

public final class Result extends Node
{
  public static final int CLASS_ID = Runtime.requestClassId("matula.io.Result");
  
  public Node data;
  public Node io;
  
  public Result(Node data, Node io)
  {
    this();

    this.data = data;
    this.io = io;
  }

  public Result(Node io)
  {
    this(new NoData(), io);
  }
  
  public Result()
  {
    super(CLASS_ID);
  }
}

package matula.io;

import matula.lang.Node;
import matula.lang.String;
import matula.runtime.Runtime;

public class FileNotFoundException extends Node
{
  public static final int CLASS_ID = Runtime.requestClassId("matula.io.FileNotFoundException");
  public final String message;
  
  public FileNotFoundException(java.io.FileNotFoundException exception)
  {
    super(CLASS_ID);
    this.message = new matula.lang.String(exception.getLocalizedMessage());
  }
}

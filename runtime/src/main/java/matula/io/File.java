package matula.io;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import matula.lang.Node;
import matula.runtime.NoMatchException;

public final class File
{

  public static final int FILE_NAME = 0;
  
  public static Node open(int arg0, Node fileName)
  {
    if(! (arg0         == FILE_NAME &&
          fileName.id  == matula.lang.String.CLASS_ID))
      throw new NoMatchException();
    
    java.io.File file = new java.io.File(fileName.toString());
    try {    
      return new IO(new FileInputStream(file), new FileOutputStream(file));
    } catch (java.io.FileNotFoundException ex) {
      return new FileNotFoundException(ex);
    }
  }
}

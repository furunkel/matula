package matula.io;

import matula.lang.Node;
import matula.runtime.Runtime;

public final class IOException extends Node
{
  public static final int CLASS_ID = Runtime.requestClassId("matula.io.IOException");
  
  public final matula.lang.String message;
  
  public IOException(java.io.IOException exception)
  {
    super(CLASS_ID);
    this.message = new matula.lang.String(exception.getLocalizedMessage());
  }
}

package matula.runtime;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicInteger;

public final class Runtime
{
  public static final int MIN_CLASS_ID = 20;
  public static ExecutorService DEFAULT_EXECUTOR_SERVICE = null;
  private static final AtomicInteger CLASS_ID_COUNTER = new AtomicInteger(MIN_CLASS_ID);

  public static int requestClassId(String className)
  {
    int classId = CLASS_ID_COUNTER.getAndIncrement();
    return classId;
  }
}

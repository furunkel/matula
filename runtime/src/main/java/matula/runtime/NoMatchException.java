package matula.runtime;

public final class NoMatchException extends RuntimeException
{
  public NoMatchException()
  {
    super();
  }

  public NoMatchException(String message)
  {
    super(message);
  }
}

package matula.lang;

import matula.runtime.Runtime;

public final class String extends Node
{
  public static final int CLASS_ID = Runtime.requestClassId("matula.lang.String");

  public final java.lang.String value;

  public String(java.lang.String value)
  {
    super(CLASS_ID);
    this.value = value;
  }

  public String(byte[] bytes)
  {
    this(new java.lang.String(bytes));
  }

  public java.lang.String toString()
  {
    return this.value;
  }

  public java.lang.String toJava()
  {
    return this.value;
  }

  public String asString()
  {
    return this;
  }
}

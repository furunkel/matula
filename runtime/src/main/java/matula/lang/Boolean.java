package matula.lang;

import matula.runtime.Runtime;

public class Boolean
{

  public static final Yes YES = new Yes();
  public static final No NO = new No();
  
  public static final class Yes extends Node
  {
    public static final int CLASS_ID = Runtime.requestClassId("matula.lang.Boolean.Yes");
    
    public Yes()
    {
      super(CLASS_ID);
    }

    public String asString()
    {
      return new String("yes");
    }
  }

  public static final class No extends Node
  {
    public static final int CLASS_ID = Runtime.requestClassId("matula.lang.Boolean.No");
    
    public No()
    {
      super(CLASS_ID);
    }

    public String asString()
    {
      return new String("no");
    }
  }
}

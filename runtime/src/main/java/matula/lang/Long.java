/*
 * This file is auto-generated.
 * Do not edit.
 *
 */

package matula.lang;

import matula.runtime.Runtime;
import matula.runtime.NoMatchException;

public final class Long extends Node
{
  public static final int CLASS_ID = Runtime.requestClassId("matula.lang.Long");
  public static final int N    = 0;
  public static final int TO   = 1;
  public static final int FROM = 2;
  public static final int BY   = 3;
  public static final int WITH = 4;

  
  private static final int MIN_CACHE = -128;
  private static final int MAX_CACHE = 127;
  private static Long[] CACHE = new Long[MAX_CACHE - MIN_CACHE + 1];
  

  public final long value;

  public Long(long value)
  {
    super(CLASS_ID);
    this.value = value;
  }

  

  public static Long valueOf(long value)
  {
    
    if (value < MIN_CACHE || value > MAX_CACHE)
        return new Long(value);

    synchronized(CACHE)
    {
      int index = (int)value - MIN_CACHE;
	    if (CACHE[index] == null)
	      CACHE[index] = new Long(value);
    
	    return CACHE[index];
    }
    
  }

  public java.lang.String toString()
  {
    return java.lang.Long.toString(this.value);
  }

  public String asString()
  {
    return new String(this.toString());
  }

  private static void matchAndReference(Node a)
  {
    if(! ((a.id == Long.CLASS_ID)))
      throw new NoMatchException();
    a.reference();
  }

  private static void matchAndReference(Node a, Node b)
  {
    if(! (a.id == Long.CLASS_ID &&
          b.id == Long.CLASS_ID))
    {
      throw new NoMatchException();
    }

    a.reference();
    b.reference();
  }

  public static Node add_n_to(Node a, Node b)
  {
    matchAndReference(a,b);
    return new Long(((Long)a).value + ((Long) b).value);
  }

  public static Node subtract_n_from(Node a, Node b)
  {
    matchAndReference(a, b);
    return new Long(((Long)a).value - ((Long) b).value);
  }

  public static Node multiply_n_by(Node a, Node b)
  {
    matchAndReference(a, b);
    return new Long(((Long)a).value * ((Long)b).value);
  }

  public static Node increment_n(Node a)
  {
    matchAndReference(a);
    return new Long(((Long)a).value + 1);
  }

  public static Node decrement(Node a)
  {
    matchAndReference(a);
    return new Long(((Long)a).value - 1);
  }

  public static Node divide_n_by(Node a, Node b)
  {
    matchAndReference(a, b);
    return new Long(((Long)a).value / ((Long)b).value);
  }

  public static Node compare_n_with( Node a, Node b)
  {
    matchAndReference(a, b);
    
    switch(java.lang.Long.compare(((Long)a).value, ((Long)b).value))
    {
      case -1:
        return Order.LESS;
      case 0:
        return Order.EQUAL;
      case 1:
        return Order.GREATER;
      default:
        throw new RuntimeException();
    }
  }

  public static Node equal_n_to(Node a, Node b)
  {
    matchAndReference(a, b);

    if(((Long)a).value == ((Long)b).value)
      return matula.lang.Boolean.YES;
    else
      return matula.lang.Boolean.NO;
  }

  public long value()
  {
    return this.value;
  }

  public java.lang.Long toJava()
  {
    return java.lang.Long.valueOf(this.value);
  }

}

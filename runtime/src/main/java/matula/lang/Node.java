package matula.lang;

import matula.lang.String;

public class Node
{
  public static final int CLASS_ID = 0;

  public final int id; 
  
  public Node(int id)
  {
    this.id = id;
  }
  
  public void reference()
  {
  }

  public String asString()
  {
    return new String(toString());
  }
}

/*
 * This file is auto-generated.
 * Do not edit.
 *
 */

package matula.lang;

import matula.runtime.Runtime;
import matula.runtime.NoMatchException;

public final class Integer extends Node
{
  public static final int CLASS_ID = Runtime.requestClassId("matula.lang.Integer");
  public static final int N    = 0;
  public static final int TO   = 1;
  public static final int FROM = 2;
  public static final int BY   = 3;
  public static final int WITH = 4;

  
  private static final int MIN_CACHE = -128;
  private static final int MAX_CACHE = 127;
  private static Integer[] CACHE = new Integer[MAX_CACHE - MIN_CACHE + 1];
  

  public final int value;

  public Integer(int value)
  {
    super(CLASS_ID);
    this.value = value;
  }

  

  public static Integer valueOf(int value)
  {
    
    if (value < MIN_CACHE || value > MAX_CACHE)
        return new Integer(value);

    synchronized(CACHE)
    {
      int index = value - MIN_CACHE;
	    if (CACHE[index] == null)
	      CACHE[index] = new Integer(value);
    
	    return CACHE[index];
    }
    
  }

  public java.lang.String toString()
  {
    return java.lang.Integer.toString(this.value);
  }

  public String asString()
  {
    return new String(this.toString());
  }

  private static void matchAndReference(Node a)
  {
    if(! ((a.id == Integer.CLASS_ID)))
      throw new NoMatchException();
    a.reference();
  }

  private static void matchAndReference(Node a, Node b)
  {
    if(! (a.id == Integer.CLASS_ID &&
          b.id == Integer.CLASS_ID))
    {
      throw new NoMatchException();
    }

    a.reference();
    b.reference();
  }

  public static Node add_n_to(Node a, Node b)
  {
    matchAndReference(a,b);
    return new Integer(((Integer)a).value + ((Integer) b).value);
  }

  public static Node subtract_n_from(Node a, Node b)
  {
    matchAndReference(a, b);
    return new Integer(((Integer)a).value - ((Integer) b).value);
  }

  public static Node multiply_n_by(Node a, Node b)
  {
    matchAndReference(a, b);
    return new Integer(((Integer)a).value * ((Integer)b).value);
  }

  public static Node increment_n(Node a)
  {
    matchAndReference(a);
    return new Integer(((Integer)a).value + 1);
  }

  public static Node decrement(Node a)
  {
    matchAndReference(a);
    return new Integer(((Integer)a).value - 1);
  }

  public static Node divide_n_by(Node a, Node b)
  {
    matchAndReference(a, b);
    return new Integer(((Integer)a).value / ((Integer)b).value);
  }

  public static Node compare_n_with( Node a, Node b)
  {
    matchAndReference(a, b);
    
    switch(java.lang.Integer.compare(((Integer)a).value, ((Integer)b).value))
    {
      case -1:
        return Order.LESS;
      case 0:
        return Order.EQUAL;
      case 1:
        return Order.GREATER;
      default:
        throw new RuntimeException();
    }
  }

  public static Node equal_n_to(Node a, Node b)
  {
    matchAndReference(a, b);

    if(((Integer)a).value == ((Integer)b).value)
      return matula.lang.Boolean.YES;
    else
      return matula.lang.Boolean.NO;
  }

  public int value()
  {
    return this.value;
  }

  public java.lang.Integer toJava()
  {
    return java.lang.Integer.valueOf(this.value);
  }

}

package matula.lang;

import java.util.concurrent.atomic.AtomicBoolean;

import matula.lang.Node;
import matula.runtime.NotLinearException;

public class LinearNode extends Node
{
  private final AtomicBoolean isReferenced = new AtomicBoolean();

  public LinearNode(int id)
  {
    super(id); 
  }
  
  public void reference()
  {
    if(isReferenced.compareAndSet(false, true) == false)
      throw new NotLinearException();
  }
}

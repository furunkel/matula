/*
 * This file is auto-generated.
 * Do not edit.
 *
 */

package matula.lang;

import matula.runtime.Runtime;
import matula.runtime.NoMatchException;

public final class Float extends Node
{
  public static final int CLASS_ID = Runtime.requestClassId("matula.lang.Float");
  public static final int N    = 0;
  public static final int TO   = 1;
  public static final int FROM = 2;
  public static final int BY   = 3;
  public static final int WITH = 4;

  

  public final float value;

  public Float(float value)
  {
    super(CLASS_ID);
    this.value = value;
  }

  

  public static Float valueOf(float value)
  {
    
      return new Float(value);
    
  }

  public java.lang.String toString()
  {
    return java.lang.Float.toString(this.value);
  }

  public String asString()
  {
    return new String(this.toString());
  }

  private static void matchAndReference(Node a)
  {
    if(! ((a.id == Float.CLASS_ID)))
      throw new NoMatchException();
    a.reference();
  }

  private static void matchAndReference(Node a, Node b)
  {
    if(! (a.id == Float.CLASS_ID &&
          b.id == Float.CLASS_ID))
    {
      throw new NoMatchException();
    }

    a.reference();
    b.reference();
  }

  public static Node add_n_to(Node a, Node b)
  {
    matchAndReference(a,b);
    return new Float(((Float)a).value + ((Float) b).value);
  }

  public static Node subtract_n_from(Node a, Node b)
  {
    matchAndReference(a, b);
    return new Float(((Float)a).value - ((Float) b).value);
  }

  public static Node multiply_n_by(Node a, Node b)
  {
    matchAndReference(a, b);
    return new Float(((Float)a).value * ((Float)b).value);
  }

  public static Node increment_n(Node a)
  {
    matchAndReference(a);
    return new Float(((Float)a).value + 1);
  }

  public static Node decrement(Node a)
  {
    matchAndReference(a);
    return new Float(((Float)a).value - 1);
  }

  public static Node divide_n_by(Node a, Node b)
  {
    matchAndReference(a, b);
    return new Float(((Float)a).value / ((Float)b).value);
  }

  public static Node compare_n_with( Node a, Node b)
  {
    matchAndReference(a, b);
    
    switch(java.lang.Float.compare(((Float)a).value, ((Float)b).value))
    {
      case -1:
        return Order.LESS;
      case 0:
        return Order.EQUAL;
      case 1:
        return Order.GREATER;
      default:
        throw new RuntimeException();
    }
  }

  public static Node equal_n_to(Node a, Node b)
  {
    matchAndReference(a, b);

    if(((Float)a).value == ((Float)b).value)
      return matula.lang.Boolean.YES;
    else
      return matula.lang.Boolean.NO;
  }

  public float value()
  {
    return this.value;
  }

  public java.lang.Float toJava()
  {
    return java.lang.Float.valueOf(this.value);
  }

}

package matula.lang;

import matula.runtime.Runtime;

public class Order
{
  public static final Greater GREATER = new Greater();
  public static final Equal   EQUAL   = new Equal();
  public static final Less    LESS    = new Less();
  
  public static final class Greater extends Node
  {
    public static final int CLASS_ID = Runtime.requestClassId("matula.lang.Order.Greater"); 

    public Greater()
    {
      super(CLASS_ID);
    }
  }

  public static final class Equal extends Node
  {
    public static final int CLASS_ID = Runtime.requestClassId("matula.lang.Order.Equal"); 

    public Equal()
    {
      super(CLASS_ID);
    }
  }

  public static final class Less extends Node
  {
    public static final int CLASS_ID = Runtime.requestClassId("matula.lang.Order.Less"); 

    public Less()
    {
      super(CLASS_ID);
    }
  }
  
  public static Node less()
  {
    return LESS;
  }

  public static Node equal()
  {
    return EQUAL;
  }

  public static Node greater()
  {
    return GREATER;
  }
}

require 'yaml'
require 'erb'

CONFIG = YAML.load_file 'config.yaml'

class Hash
  def method_missing(method, *args, &block)
    key = method.to_s
    if key?(key)
      self[key]
    else
      super
    end
  end
end

repositories.remote << 'http://repo1.maven.org/maven2'

JCOMMANDER = 'com.beust:jcommander:jar:1.27'
ASM = 'org.ow2.asm:asm-all:jar:4.0'

Project.local_task :run

define 'matula' do
  project.version = '0.1'

  desc 'Runtime'
  define 'runtime' do
    package :jar

    desc 'generate Block class'
    task :generate_block_class do

      max_args = CONFIG.runtime.generate.block_class.max_args
      max_blocks = CONFIG.runtime.generate.block_class.max_blocks

      block_template_filename = Dir['runtime/src/**/Block.java.erb'].first
      output_filename = block_template_filename.sub('.erb', '')

      File.open(output_filename, 'w') do |file|
        data = File.read(block_template_filename)
        template = ERB.new(data)
        file.write template.result(binding)
      end
    end

    desc 'generate numeric classes'
    task :generate_numeric_classes do

      numeric_classes = CONFIG.runtime.generate.numeric_classes
      
      numeric_classes.each do |numeric_class|
        
        block_template_filename = Dir['runtime/src/**/Numeric.erb'].first
        output_filename = block_template_filename.sub('Numeric.erb', numeric_class.class_name + '.java')

        File.open(output_filename, 'w') do |file|
          data = File.read(block_template_filename)
          template = ERB.new(data)
           
          file.write template.result(numeric_class.instance_eval{binding})
        end
      end

    end
  end

  desc 'Compiler'
  define 'compiler' do
    compile.with JCOMMANDER, ASM, project('runtime')

    manifest_opts = {'Main-Class' => 'org.matula.compile.Compiler'}

    package(:jar).with :manifest => manifest_opts

    run.using :main => (['org.matula.compile.Compiler'] + (ENV['params'] || '').split(' '))
  end
end

package org.matula.compile;

import org.matula.compile.ast.Ast;
import org.matula.compile.ast.Edge;
import org.matula.compile.ast.Module;
import org.matula.compile.ast.Node;
import org.matula.compile.ast.Rule;

public abstract class Analyzer {

  
  public void analyze(Ast ast) throws Exception
  {
    analyzeAst(ast);
  }

  protected void analyzeAst(Ast ast) throws Exception
  {
    analyzeImports();
    analyzeModule(ast.getModule());
  }

  protected void analyzeImports()
  {
  }

  protected void analyzeModule(Module module) throws Exception
  {
    for(Rule rule: module.getRules())
    {
      analyzeRule(rule);
    }
  }

  protected void analyzeRule(Rule rule) throws Exception
  {
    for(Node node: rule.getNodes())
    {
      analyzeNode(node);
    }
  }

  public void analyzeNode(Node node) throws Exception
  {
  }

  public static void analyze(Ast ast, Analyzer... analyzers) throws Exception
  {
    for(Analyzer analyzer: analyzers)
    {
      analyzer.analyze(ast);
    }
  }
}

package org.matula.compile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.matula.compile.ast.Ast;
import org.matula.compile.ast.Module;
import org.matula.compile.parsing.ParseException;
import org.matula.compile.parsing.Parser;

public class ModuleLoader
{
  private static final String MODULE_FILE_EXTENSION = "mat";
  public static final ModuleLoader INSTANCE = new ModuleLoader();

  private File baseDirectory;
  private final Map<String, Module> loaded;

  public ModuleLoader()
  {
    this.loaded = new HashMap<String, Module>();
  }

  public Module load(String moduleName) throws FileNotFoundException, IOException, ParseException
  {
    Module module = loaded.get(moduleName);
    if(module != null)
    {
      return module;
    }
    else
    {
      File file = moduleFile(moduleName);
      Parser parser = new Parser(file);
      parser.parse();
       
      Ast ast = parser.getAst();
      module = ast.getModule();

      loaded.put(moduleName, module);
      return module;
    }
  }

  protected void setBaseDirectory(File baseDirectory)
  {
    this.baseDirectory = baseDirectory;
  }

  private File moduleFile(String moduleName)
  {
    return new File(this.baseDirectory, String.format("%s.%s", moduleName.replace(Constants.MODULE_NAME_SEPARATOR, File.separatorChar), MODULE_FILE_EXTENSION));
  }
}

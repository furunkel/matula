package org.matula.compile;

import org.matula.compile.parsing.Parser;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.matula.compile.ast.Ast;
import org.matula.compile.ast.Edge;
import org.matula.compile.ast.Rule;
import org.matula.compile.parsing.ParseException;
import org.matula.compile.ast.Module;

public class Compiler
{

  private static final String DEFAULT_THREAD_POOL_TYPE = ClassGenerator.ThreadPoolType.FIXED.toString();
 
  @Parameter(names = "-d", description = "Output directory", required = true)
  private String outputDirectory;

  @Parameter(names = "-b", description = "Base directory", required = true)
  private String baseDirectory;
  
  @Parameter(names = "-verify", description = "Verify bytecode")
  private boolean verify = false;

  @Parameter(names = "-e", description = "Wheter to use a 'fixed' 'cached' thread pool")
  private String threadPoolType = DEFAULT_THREAD_POOL_TYPE;

  @Parameter(names = "-s", description = "Size of the thread pool")
  private Integer nThreads = -1;

  @Parameter(description = "Modules", required = true)
  private List<String> moduleNames = new ArrayList<String>();

  
  private static final String MAIN_RULE_NAME = "main";
 
  private long totalParseTime = 0;
  private long totalGenerateTime = 0;
  
  public void compile() throws Exception
  {
    File baseDirectoryFile = new File(baseDirectory);
    if(!baseDirectoryFile.isDirectory())
    {
      throw new FileNotFoundException(baseDirectoryFile.getAbsolutePath());
    }
    ModuleLoader.INSTANCE.setBaseDirectory(baseDirectoryFile);
      
    List<Module> modules = new ArrayList<Module>();
    for(String moduleName: moduleNames)
    {
      Module module = ModuleLoader.INSTANCE.load(moduleName);

      modules.add(module);
    }

    long generateTime = System.currentTimeMillis();

    for(Module module: modules)
    {
      ClassGenerator generator = new ClassGenerator(module, new ClassGenerator.Callback() {
        public void call(String name, byte[] bytecode) {
          try
          {
            write(name, bytecode);
          }
          catch(Exception e)
          {
            e.printStackTrace();
          }
        }
      });

      Map<List<Object>, List<Rule>> ruleSets = new HashMap<List<Object>, List<Rule>>();

      for(Rule rule: module.getRules())
      {
        List<Object> key = new ArrayList<Object>(Arrays.asList(rule.getPattern().getLabel(),
                                                 Util.incomingEdgeLabels(rule.getPattern())));
                                                 /*(Integer)rule.getBlockParameters().size()));*/

        List<Rule> rulesForKey = ruleSets.get(key);
        if(rulesForKey == null)
        {
          rulesForKey = new ArrayList<Rule>();
          ruleSets.put(key, rulesForKey);
        }
        
        rulesForKey.add(rule);
      }

      for(List<Object> key: ruleSets.keySet())
      {
        List<Rule> rules = ruleSets.get(key);
        
        String name = (String) key.get(0);
        
        if(isMainRule(name))
        {
          generator.visitMainRuleSet(rules, ClassGenerator.ThreadPoolType.valueOf(this.threadPoolType.toUpperCase()), this.nThreads);
        }
        else
        {
          generator.visitRuleSet(rules);
        }
      }

      generator.visitEnd();
      write(module.getInternalName(), generator.toByteArray());

      totalGenerateTime = System.currentTimeMillis() - generateTime;
    }
  }

  private boolean isMainRule(String name)
  {
    return Labels.ruleName(name).equals(MAIN_RULE_NAME);  
  }

  private void write(String className, byte[] bytecode) throws FileNotFoundException, IOException
  {
    File outputDirectoryFile = new File(outputDirectory);
    File classFile = new File(outputDirectoryFile, className + ".class");
    File classParentFile = classFile.getParentFile();

    classParentFile.mkdirs();
    classFile.createNewFile();

    new FileOutputStream(classFile).write(bytecode);
  
    if(verify)
    {
      ClassGenerator.verify(bytecode);
    }
  }
  
  public static void main(String[] args) throws Exception
  {
    Compiler compiler = new Compiler();

    new JCommander(compiler, args);


    compiler.compile();
  }

}

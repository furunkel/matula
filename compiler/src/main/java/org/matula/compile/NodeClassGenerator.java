package org.matula.compile;

import java.util.Collections;
import java.util.List;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import org.matula.compile.ast.Edge;
import org.matula.compile.ast.Module;
import org.matula.compile.ast.Node;
import org.matula.compile.ast.RuleReference;

public final class NodeClassGenerator implements Opcodes
{
  private ClassWriter classWriter;
  private String className;
  
  public NodeClassGenerator(Module module, Node node)
  {
    this.classWriter = new ClassWriter(ClassWriter.COMPUTE_FRAMES | ClassWriter.COMPUTE_MAXS);

    RuleReference ruleReference = node.toRuleReference(module);

    this.className = className(ruleReference);

    classWriter.visit(V1_7,
                      ACC_PUBLIC | ACC_FINAL | ACC_SUPER,
                     this.className,
                     null,
                     "matula/lang/Node",
                     null);

    classWriter.visitOuterClass(outerClassName(ruleReference), null, null);

    classWriter.visitField(ACC_PUBLIC + ACC_FINAL + ACC_STATIC, "CLASS_ID", "I", null, new Integer(-1)).visitEnd();

    visitClinit();
    visitConstructor(node);
    //classWriter.visitInnerClass(name, outerClassName, null, ACC_STATIC | ACC_FINAL | ACC_SUPER);
    visitFields(node);
  }

  public void visitEnd()
  {
    this.classWriter.visitEnd();
  }

  public byte[] toByteArray()
  {
    return this.classWriter.toByteArray();
  }
  
  private void visitClinit()
  {
    MethodVisitor mv = classWriter.visitMethod(ACC_STATIC, "<clinit>", "()V", null, null);
    mv.visitCode();

    mv.visitLdcInsn(Util.canoncialModuleName(this.className));
    mv.visitMethodInsn(INVOKESTATIC, "matula/runtime/Runtime", "requestClassId" , "(Ljava/lang/String;)I");
    mv.visitFieldInsn(PUTSTATIC, this.className, "CLASS_ID", "I"); 

    mv.visitInsn(RETURN);
    mv.visitMaxs(0, 0);
    mv.visitEnd();
  }

  public static String constructorDescriptor(int n)
  {
    StringBuilder builder = new StringBuilder("(");
    for(int i = 0; i < n; i++)
    {
      builder.append("Lmatula/lang/Node;"); 
    }
    return builder.append(")V").toString();
  }
  
  private void visitConstructor(Node node)
  {
    String methodDescriptor = constructorDescriptor(node.getIncoming().size()); 
    
    MethodVisitor mv = classWriter.visitMethod(ACC_PUBLIC, "<init>", methodDescriptor, null, null);
    mv.visitCode();
    
    mv.visitVarInsn(ALOAD, 0);
    mv.visitFieldInsn(GETSTATIC, this.className, "CLASS_ID", "I"); 
    mv.visitMethodInsn(INVOKESPECIAL, "matula/lang/Node", "<init>", "(I)V");

    int index = 0;
    for(Edge edge: node.getIncoming())
    {
      mv.visitVarInsn(ALOAD, 0);
      mv.visitVarInsn(ALOAD, index + 1);
      mv.visitFieldInsn(PUTFIELD, className, Util.edgeLabelToFieldName(edge.getLabel()), "Lmatula/lang/Node;");
      index++;
    }
    
    mv.visitInsn(RETURN);
    
    mv.visitMaxs(0, 0);
    mv.visitEnd();
  }

  private static String innerClassName(RuleReference ruleReference)
  {
    return Util.capitalize(ruleReference.getInternalRuleName(true)); 
  }

  private static String outerClassName(RuleReference ruleReference)
  {
    return ruleReference.getInternalModuleName(); 
  }

  public static String className(RuleReference ruleReference)
  {
    return outerClassName(ruleReference) + "$" + innerClassName(ruleReference);
  }

  private void visitFields(Node node)
  {
    for(Edge edge: node.getIncoming())
    {
      classWriter.visitField(ACC_PUBLIC | ACC_FINAL, Util.toLowerCamelCase(edge.getLabel()), "Lmatula/lang/Node;", null, null); 
    }
  }
}

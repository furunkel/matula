package org.matula.compile;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.matula.compile.ast.BlockNode;
import org.matula.compile.ast.BlockArgument;
import org.matula.compile.ast.Edge;
import org.matula.compile.ast.Module;
import org.matula.compile.ast.Node;
import org.matula.compile.ast.Rule;
import org.matula.compile.ast.RuleReference;
import org.matula.compile.ast.UnparameterizedRuleReference;
import org.matula.compile.parsing.ParseException;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public class CallGraphGenerator implements Opcodes
{

  public static void generate(Module module, MethodVisitor mv, Rule rule, Node node, Map<Node, Integer> varIndexMap, int[] varId) throws Exception
  {
    generate(module, mv, rule, node, varIndexMap, varId, new HashSet<Integer>());  
  }
  
  public static void generate(Module module, MethodVisitor mv, Rule rule, Node node, Map<Node, Integer> varIndexMap, int[] varId, Set<Integer> futureIds) throws Exception
  {
    if(varIndexMap.get(node) != null)
      return;

    if(node.isInteger())
    {
      generateInteger(mv, node.asInteger());
      mv.visitVarInsn(ASTORE, varId[0]);

      varIndexMap.put(node, varId[0]);
      varId[0] += 1;
    }
    else if(node.isLong())
    {
      Long lng = node.asLong();

      generateLong(mv, lng);
      mv.visitVarInsn(ASTORE, varId[0]);

      varIndexMap.put(node, varId[0]);
      varId[0] += 1;
    }
    else if(node.isDouble())
    {
      Double dbl = node.asDouble();

      generateDouble(mv, dbl);
      mv.visitVarInsn(ASTORE, varId[0]);

      varIndexMap.put(node, varId[0]);
      varId[0] += 1;
    }
    else if(node.isFloat())
    {
      Float flt = node.asFloat();

      generateFloat(mv, flt);
      mv.visitVarInsn(ASTORE, varId[0]);

      varIndexMap.put(node, varId[0]);
      varId[0] += 1;
    }
    else if(node.isString())
    {
      String string = node.asString();

      generateString(mv, string);
      mv.visitVarInsn(ASTORE, varId[0]);

      varIndexMap.put(node, varId[0]);
      varId[0] += 1;
    }
    else if(node.isCharacter())
    {
      Character character = node.asCharacter();

      generateCharacter(mv, character);
      mv.visitVarInsn(ASTORE, varId[0]);

      varIndexMap.put(node, varId[0]);
      varId[0] += 1;
    }
    else
    { 
      generateCallGraph(module, mv, rule, node, varIndexMap, varId, futureIds);
    }
  }

  private static void generateCallGraph(Module module, MethodVisitor mv, Rule rule, Node node, Map<Node, Integer> varIndexMap, int[] varId, Set<Integer> futureIds) throws Exception
  {
      //List<Pair<String, String>> path = pathMapping.get(id);
      if(node.getFlags().contains(Node.Flag.PATTERN_REACHABLE))
      {
        int[] path = rule.patternPath(node);

        mv.visitVarInsn(ALOAD, path[0]);
        generatePath(mv, module, rule, rule.getPattern().getIncoming().get(path[0]).getNode(), path, 1);

        mv.visitInsn(DUP);
        mv.visitMethodInsn(INVOKEVIRTUAL, "matula/lang/Node", "reference", "()V");
        mv.visitVarInsn(ASTORE, varId[0]);
        
        varIndexMap.put(node, varId[0]);
        varId[0] += 1;
      }
      else
      {
        RuleReference ruleReference = node.toRuleReference(module);

        if(node.isForking())
        {
          mv.visitTypeInsn(NEW, CallableClassGenerator.className(module, ruleReference));
          mv.visitInsn(DUP);
        }

        for(Edge edge: node.getIncoming())
        {
          Node edgeNode = edge.getNode();
          generate(module, mv, rule, edgeNode, varIndexMap, varId, futureIds);
        }

        if(node.isBlock())
        {
          mv.visitVarInsn(ALOAD, rule.arity() + (rule.getBlockParameters().indexOf(node)));
        }

        for(Edge edge: node.getIncoming())
        {
          String edgeLabel = edge.getLabel();
          Node edgeNode = edge.getNode();
          
          Integer id = varIndexMap.get(edgeNode);
          mv.visitVarInsn(ALOAD, id);

          if(futureIds.contains(id))
          {
            mv.visitMethodInsn(INVOKEINTERFACE, "java/util/concurrent/Future", "get", "()Ljava/lang/Object;");
            mv.visitTypeInsn(CHECKCAST, Constants.NODE_INTERNAL_NAME);
             
          }
        }


        int index = 0;
        for(BlockArgument blockArgument: node.getBlockArguments())
        {
           if(blockArgument.get() instanceof BlockNode)
           {
             mv.visitVarInsn(ALOAD, rule.arity() + blockArgument.getIndex());
           }
           else
           {
             UnparameterizedRuleReference blockRuleReference = (UnparameterizedRuleReference) blockArgument.get();
               
             String blockConstantName = ClassGenerator.blockConstantName(module, blockRuleReference,
                                                                         ruleReference.getBlockParameters(index));
             
             // Load static block constant
             mv.visitFieldInsn(GETSTATIC, module.getInternalName(), blockConstantName, "Lmatula/lang/Block;");
           }
           index++;
        }


        if(!node.isForking())
        {
          if(!node.isBlock())
          {
            mv.visitMethodInsn(INVOKESTATIC,
                               ruleReference.getInternalModuleName(),
                               ruleReference.getInternalRuleName(),
                               ruleReference.getDescriptor(node.blockArity()));
          }
          else
          {
            BlockNode block = (BlockNode)node;
            mv.visitMethodInsn(INVOKEVIRTUAL,
                               Constants.BLOCK_INTERNAL_NAME,
                               "call",
                               ruleReference.getDescriptor(node.blockArity()));
          }
        }
        else
        {
          //FIXME: block and forking !!

          mv.visitMethodInsn(INVOKESPECIAL, CallableClassGenerator.className(module, ruleReference), "<init>", Util.methodDescriptor(node.arity(), node.blockArity(), "V", node.isBlock()));
          mv.visitFieldInsn(GETSTATIC, Constants.RUNTIME_INTERNAL_NAME, Constants.DEFAULT_EXECUTOR_SERVICE_CONSTANT_NAME, "Ljava/util/concurrent/ExecutorService;");
          mv.visitInsn(SWAP);
          mv.visitMethodInsn(INVOKEINTERFACE, "java/util/concurrent/ExecutorService", "submit",
                              "(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;");
          futureIds.add(varId[0]);
        }

        mv.visitVarInsn(ASTORE, varId[0]);
        varIndexMap.put(node, varId[0]);
        varId[0] += 1;
      }
  }

  private static void generatePath(MethodVisitor mv, Module module, Rule rule, Node node, int[] path, int index)
  {
    if(index < path.length)
    {
      Edge edge= node.getIncoming().get(path[index]);

      RuleReference ruleReference = node.toRuleReference(module);
      
      String edgeLabel = edge.getLabel();

      String className = NodeClassGenerator.className(ruleReference);
      mv.visitTypeInsn(CHECKCAST, className);
      mv.visitFieldInsn(GETFIELD, className, Util.edgeLabelToFieldName(edgeLabel), "Lmatula/lang/Node;");

      generatePath(mv, module, rule, edge.getNode(), path, index + 1);
    }
  }

  public static void generateInteger(MethodVisitor mv, int value)
  {
    mv.visitLdcInsn(value);
    mv.visitMethodInsn(INVOKESTATIC, "matula/lang/Integer", "valueOf", "(I)Lmatula/lang/Integer;");
  }

  public static void generateLong(MethodVisitor mv, long value)
  {
    mv.visitLdcInsn(value);
    mv.visitMethodInsn(INVOKESTATIC, "matula/lang/Long", "valueOf", "(J)Lmatula/lang/Long;");
  }

  private static void generateDouble(MethodVisitor mv, double value)
  {
    mv.visitLdcInsn(value);
    mv.visitMethodInsn(INVOKESTATIC, "matula/lang/Double", "valueOf", "(D)Lmatula/lang/Double;");
  }

  private static void generateFloat(MethodVisitor mv, float value)
  {
    mv.visitLdcInsn(value);
    mv.visitMethodInsn(INVOKESTATIC, "matula/lang/Float", "valueOf", "(F)Lmatula/lang/Float;");
  }

  private static void generateString(MethodVisitor mv, String value)
  {
    mv.visitTypeInsn(NEW, "matula/lang/String");
    mv.visitInsn(DUP);
    mv.visitLdcInsn(value);
    mv.visitMethodInsn(INVOKESPECIAL, "matula/lang/String", "<init>", "(Ljava/lang/String;)V");
  }

  private static void generateCharacter(MethodVisitor mv, Character value)
  {
    mv.visitTypeInsn(NEW, "matula/lang/Character");
    mv.visitInsn(DUP);
    mv.visitLdcInsn(value);
    mv.visitMethodInsn(INVOKESPECIAL, "matula/lang/Character", "<init>", "(C)V");
  }
  
}

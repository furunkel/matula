package org.matula.compile;

public class Labels
{
  public static String[] split(String label)
  {
    return split(label, null);
  }

  public static String[] split(String label, String fallbackModuleName)
  {
    String[] parts = label.split("\\.");
    if(parts.length < 2)
      return new String[]{fallbackModuleName, parts[0]};

    StringBuilder moduleBuilder = new StringBuilder();
    for(int i = 0; i < parts.length - 1; i ++)
    {
       moduleBuilder.append(parts[i]);
       if(i < parts.length - 2)
       {
         moduleBuilder.append('.');
       }
    }
    String moduleName = moduleBuilder.toString();
    String ruleName = parts[parts.length - 1];

    return new String[]{moduleName, ruleName};
  }

  public static String moduleName(String label)
  {
    return split(label)[0];
  }

  public static String ruleName(String label)
  {
    return split(label)[1];
  }


}

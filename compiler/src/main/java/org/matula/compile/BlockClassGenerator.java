package org.matula.compile;

import java.util.List;
import org.matula.compile.ast.Module;
import org.matula.compile.ast.RuleReference;
import org.matula.compile.ast.UnparameterizedRuleReference;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public final class BlockClassGenerator implements Opcodes
{
  private ClassWriter classWriter;
  private String      className;

  
  public BlockClassGenerator(Module module, UnparameterizedRuleReference ruleReference, RuleReference.BlockParameters blockParameters) throws Exception
  {
    this.classWriter = new ClassWriter(classWriter.COMPUTE_FRAMES | ClassWriter.COMPUTE_MAXS);

    className = className(module, ruleReference, blockParameters);
    String outerClassName = module.getInternalName();

    classWriter.visit(V1_7,
                      ACC_PUBLIC | ACC_SUPER,
                      className,
                      null,
                      "matula/lang/Block",
                      null);

    classWriter.visitOuterClass(outerClassName, null, null);

    visitConstructor();
    
    //classWriter.visitInnerClass(className, null, null, ACC_PUBLIC | ACC_FINAL | ACC_STATIC);
   
    {
      
      String descriptor = Util.methodDescriptor(blockParameters.arity(), blockParameters.blockArity());
      MethodVisitor mv = classWriter.visitMethod(ACC_PUBLIC, "call", descriptor, null, null);
      mv.visitCode();

      int edgeIndex = 0;
      for(String parameterName: blockParameters.getParamterNames())
      {
        // remember: 0 is this here.
        mv.visitVarInsn(ALOAD, edgeIndex + 1);
        
        edgeIndex++;
      }

      for(int i = 0; i < blockParameters.blockArity(); i++)
      {
        mv.visitVarInsn(ALOAD, (blockParameters.arity() + i) + 1);
      }

      RuleReference blockRuleReference = new RuleReference(ruleReference, blockParameters.getParamterNames());
      
      String staticDescriptor = Util.methodDescriptor(blockParameters.arity(), blockParameters.blockArity());
      mv.visitMethodInsn(INVOKESTATIC, blockRuleReference.getInternalModuleName(), blockRuleReference.getInternalRuleName() , staticDescriptor);

      mv.visitInsn(ARETURN);
      
      mv.visitMaxs(0, 0);
      mv.visitEnd();
    }
  }

  private void visitConstructor()
  {
    MethodVisitor mv = classWriter.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
    mv.visitCode();
    
    mv.visitVarInsn(ALOAD, 0);
    mv.visitMethodInsn(INVOKESPECIAL, "matula/lang/Block", "<init>", "()V");
    mv.visitInsn(RETURN);
    
    mv.visitMaxs(0, 0);
    mv.visitEnd();
  }

  private static String innerClassName(UnparameterizedRuleReference ruleReference, RuleReference.BlockParameters blockParameters)
  {
    StringBuilder parametersStringBuilder = new StringBuilder();
    for(String parameterName: blockParameters.getParamterNames())
    {
      parametersStringBuilder.append(parameterName);
      parametersStringBuilder.append('_');
    }
    parametersStringBuilder.deleteCharAt(parametersStringBuilder.length() - 1);
    
    String parameterString = parametersStringBuilder.toString();
    
    return String.format("Block_%s__%s", Util.toUpperCamelCase(ruleReference.getRuleName()), parameterString); 
  }
  
  public static String className(Module module, UnparameterizedRuleReference ruleReference, RuleReference.BlockParameters blockParameters)
  {
    return String.format("%s$%s", module.getInternalName(), innerClassName(ruleReference, blockParameters));
  }

  public String getClassName()
  {
    return className;
  }

  public void visitEnd()
  {
    this.classWriter.visitEnd();
  }

  public byte[] toByteArray()
  {
    return this.classWriter.toByteArray();
  }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.matula.compile.parsing;

/**
 *
 * @author user
 */
public class SimpleParseException extends ParseException
{
  private final String message;
  
  public SimpleParseException(String message, Parser parser)
  {
    super(parser);
    this.message = message;
  }

  @Override
  public String getMessage()
  {
    return getMessage(this.message);
  }
}

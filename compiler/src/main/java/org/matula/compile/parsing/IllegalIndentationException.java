package org.matula.compile.parsing;

import java.util.Arrays;
import java.util.Set;

public class IllegalIndentationException extends ParseException
{
  private final int indentation;
  private final OffsetTable offsetTable;
  private final int defaultIndentation;
  
  public IllegalIndentationException(int indentation, Parser parser)
  {
    super(parser); 
    this.indentation = indentation;
    
    this.offsetTable = parser.getCurrentOffsetTable();
    this.defaultIndentation = parser.getDefaultIndentation();
  }

  @Override
  public String getMessage()
  {
    Set<Integer> offsets = offsetTable.offsets();
    
    StringBuilder possibleIndentationsBuilder = new StringBuilder();
    String orSeparator = " or ";
    for(Integer offset: offsets)
    {
      possibleIndentationsBuilder.append(offset + 1);
      possibleIndentationsBuilder.append(" for ");
      possibleIndentationsBuilder.append(offsetTable.getNode(offset).getLabel());
      possibleIndentationsBuilder.append(orSeparator);
    }
    if(possibleIndentationsBuilder.length() > 0)
    {
      possibleIndentationsBuilder.delete(possibleIndentationsBuilder.length() - orSeparator.length(), possibleIndentationsBuilder.length());
    }

    return getMessage(String.format("wrong indentation level: %d instead of %s", this.indentation, possibleIndentationsBuilder.toString()));
  }
  
}

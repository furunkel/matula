package org.matula.compile.parsing;

import java.util.HashMap;
import java.util.Map;
import org.matula.compile.ast.Node;


public class SymbolTable
{
  private Map<String, Node> map;
  private Parser parser;

  public SymbolTable(Parser parser)
  {
    this.parser = parser;
    this.map = new HashMap<String, Node>();
  }

  public Node get(String key) throws ParseException
  {
    if(!map.containsKey(key))
    {
      throw new SimpleParseException(String.format("no such symbol: %s", key), parser);
    }
    else
    {
      return map.get(key);
    }
  }

  public Node set(String key, Node node) throws ParseException
  {
    if(map.containsKey(key))
    {
      throw new SimpleParseException(String.format("%s already defined", key), parser);
    }
    else
    {
      return map.put(key, node);
    }
  }
}
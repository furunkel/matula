package org.matula.compile.parsing;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.matula.compile.ast.Node;

public class OffsetTable
{
  private Map<Integer, Node> nodes;
  private Map<Node, Integer> offsets;

  public OffsetTable()
  {
    this.nodes = new HashMap<Integer, Node>();
    this.offsets = new HashMap<Node, Integer>();
  }
  
  public Node getNode(Integer offset)
  {
    return this.nodes.get(offset);
  }
  
  public Integer getOffset(Node node)
  {
    return this.offsets.get(node);
  }

  public void set(Integer offset, Node node)
  {
    this.nodes.put(offset, node);
    this.offsets.put(node, offset);
  }

  public Set<Integer> offsets()
  {
    return this.nodes.keySet();
  }
          
}

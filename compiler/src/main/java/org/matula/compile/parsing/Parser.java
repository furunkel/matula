package org.matula.compile.parsing;

import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import org.matula.compile.Constants;
import org.matula.compile.Labels;
import org.matula.compile.ast.Edge;

import org.matula.compile.ast.Node;
import org.matula.compile.ast.Ast;
import org.matula.compile.ast.BlockArgument;
import org.matula.compile.ast.BlockNode;
import org.matula.compile.ast.Module;
import org.matula.compile.ast.Rule;
import org.matula.compile.ast.UnparameterizedRuleReference;


public class Parser {

  private static final String EDGE_SUFFIX    = ":";
  private static final String BIND_PREFIX    = "@";
  private static final String COMMENT_PREFIX = "#";

  private static final String NODE_REFERENCE_PREFIX     = "@";
  private static final String BLOCK_REFERENCE_PREFIX    = "&";
  private static final String MULTI_LINE_COMMENT_PREFIX = "###";
  private static final String FORK_PREFIX = "*";

  private static final String INVALID_CLASS_NAME_MESSAGE = "invalid class name %s";
  private static final int DEFAULT_INDENT = 1;
  
  private static Pattern p(String pattern)
  {
    return Pattern.compile(pattern);
  }

  private static String s(Pattern pattern)
  {
    return pattern.pattern().substring(1);
  }
  
  private static final Pattern BLOCK_NAME = p("^[a-zA-Z][a-zA-Z0-9\\-]*");
  private static final Pattern BIND_NAME  = p("^[a-zA-Z][a-zA-Z0-9\\-]*");
  private static final Pattern EDGE_LABEL = p("^[a-zA-Z][a-zA-Z0-9]*");
  private static final Pattern NODE_LABEL = p("^(?>_|(?>[a-zA-Z][a-zA-Z0-9\\-\\'\"\\.]*)|(?>\\d+))");

  private static final Pattern EDGE = p("^(\\" + FORK_PREFIX + ")?(" + s(EDGE_LABEL) + ")" + EDGE_SUFFIX);
  private static final Pattern BIND = p("^" + BIND_PREFIX + "(" + s(BIND_NAME) + ")");
  
  private static final Pattern IMPORT_KEYWORD = p("^import ");
  private static final Pattern MODULE_KEYWORD = p("^module ");
  private static final Pattern TO_KEYWORD     = p("^to ");
  private static final Pattern MODULE_NAME    = p("^(?:[a-z]+\\.)*(?:[A-Z][a-zA-Z0-9]*)");
  
  private static final Pattern NODE_REFERENCE     = p("^" + NODE_REFERENCE_PREFIX + "(" + s(BIND_NAME) + ")");
  private static final Pattern BLOCK_REFERENCE    = p("^" + BLOCK_REFERENCE_PREFIX +  "(" + s(BLOCK_NAME) + ")");
  private static final Pattern BLOCK_PARAMS_PAREN = p("^\\|");
  private static final Pattern BLOCK_ARGS_PAREN   = BLOCK_PARAMS_PAREN;
  private static final Pattern FORK = p("^\\*");


  private static class ImportTable extends HashMap<String, String>{}

  private static class ParseFailedException extends RuntimeException
  {
    private List<ParseException> exceptions;
    
    public ParseFailedException(List<ParseException> exceptions)
    {
      this.exceptions = exceptions;  
    }

    @Override
    public void printStackTrace(PrintWriter printWriter)
    {
      super.printStackTrace(printWriter); 
      for(Exception e: exceptions)
      {
        e.printStackTrace(printWriter);
      }
    }

    @Override
    public void printStackTrace(PrintStream printStream)
    {
      super.printStackTrace(printStream); 
      for(Exception e: exceptions)
      {
        e.printStackTrace(printStream);
      }
    }
  }

  private static final Comparator<Edge> EDGE_COMPARATOR = new Comparator<Edge>(){
    public int compare(Edge a, Edge b)
    {
      return a.getLabel().compareTo(b.getLabel());
    }
  };
  
  private final Ast ast;
  private final File file;
  private final LineNumberReader reader;
  private final ImportTable importTable;
  private final int defaultIndentation;

  private String line;
  private boolean inMultiLineComment;
  private Module module;
  private Rule rule;
  private SymbolTable symbolTable;
  private OffsetTable offsetTable;
  private Node pattern;
  private Node replacement;
  private Scanner scanner;
  
  public Parser(File file, int indent) throws FileNotFoundException
  {
    this.file = file;
    this.defaultIndentation = indent;
    this.reader = new LineNumberReader(new FileReader(this.file));
    this.ast = new Ast();
    this.importTable = new ImportTable();
  }

  public Parser(File file) throws FileNotFoundException
  {
    this(file, DEFAULT_INDENT);
  }

  public OffsetTable getCurrentOffsetTable()
  {
    return this.offsetTable; 
  }

  public int getDefaultIndentation()
  {
    return this.defaultIndentation;
  }

  public void parse() throws IOException, ParseException
  {
    try {
      nextLine();
      parseImports();
      parseClass();
    } catch(EOFException e)
    {

    }

    postProcess();
  }

  public Ast getAst()
  {
    return ast;
  }

  public File getFile()
  {
    return file;
  }

  public int getLineNumber()
  {
    return reader.getLineNumber();
  }

  public int getColumnNumber()
  {
    return scanner.offset();
  }

  private void parseImports() throws ParseException, IOException
  {
     scanner = new Scanner(line);
     if(scanner.hasNext(IMPORT_KEYWORD))
     {
       scanner.next();
       if(scanner.hasNext(MODULE_NAME))
       {
         String moduleName = scanner.next();

         if(importTable.containsValue(moduleName))
         {
           throw new SimpleParseException(String.format("%s imported multiple times", moduleName), this);
         }

         String[] parts = moduleName.split("\\" + Constants.MODULE_NAME_SEPARATOR);
         String lastPart = parts[parts.length - 1];

         if(importTable.containsKey(lastPart))
         {
           throw new SimpleParseException(String.format("import %s conflicts with import of %s", moduleName, importTable.get(lastPart)), this);
         }
         else
         {
           importTable.put(lastPart, moduleName);
         }

         nextLine();
         parseImports();
       }
       else
       {
         throw new SimpleParseException(String.format(INVALID_CLASS_NAME_MESSAGE, scanner.next()), this);
       }
     }
  }

  private void parseClass() throws ParseException, IOException
  {
     module = new Module();
     parseModuleDeclaration(); 

     ast.setModule(module);

     parseRules();
  }

  private void parseModuleDeclaration() throws ParseException, IOException
  {
     scanner = new Scanner(line);
     if(scanner.hasNext(MODULE_KEYWORD))
     {
       scanner.next();
       if(scanner.hasNext(MODULE_NAME))
       {
         String moduleName = scanner.next();
         module.setName(moduleName);

         nextLine();
       }
       else
       {
         throw new SimpleParseException(String.format(INVALID_CLASS_NAME_MESSAGE, scanner.next()), this);
       }
     }
     else
     {
       throw new SimpleParseException("no module declaration found", this);
     }
  }

  private void parseRules() throws ParseException, IOException
  {
    parseRule();
    System.out.println("rule");
    parseRules();
  }
  
  private void parseRule() throws ParseException, IOException
  {
    rule = new Rule();
    symbolTable = new SymbolTable(this);
    
    parsePattern();
    parseReplacement();  

    module.getRules().add(rule);
  }
  
  /*private void parseRules() throws ParseException, IOException
  {
    parseRule(null);
  }
  
  private void parseRule(ParseException... exceptions) throws ParseException, IOException
  {
    rule = new Rule();
    symbolTable = new SymbolTable(this);
    
    try { 
      parsePattern();
    } catch(ParseException e)
    {
      if(exceptions != null)
      {
        List<ParseException> exceptionList = new ArrayList<ParseException>(Arrays.asList(exceptions));
        throw new ParseFailedException(exceptionList);
      }
      else if(e instanceof IllegalIndentationException)
      {
        try {
          parseReplacement();  
        } catch (IllegalReplacementException | IllegalIndentationException | IllegalEdgeException e2)
        {
          module.getRules().add(rule);

          parseRule(e, e2);

        } catch(EOFException eof)
        {
          module.getRules().add(rule);
          throw eof;
        }
      }
      else
      {
        throw e;
      }
    }
  }*/

  private void parsePattern() throws ParseException, IOException
  {
    offsetTable = new OffsetTable();

    parsePatternSink();

    rule.setPattern(pattern);
    rule.getNodes().add(pattern);
    
    parseEdges();
  }

  private void parsePatternSink() throws ParseException, IOException
  {
    scanner = new Scanner(line, false);
   
    if(scanner.hasNext(NODE_LABEL))
    {
      String nodeLabel = scanner.next();
      
      pattern = new Node(nodeLabel(nodeLabel));
      offsetTable.set(0, pattern);
              
      if(scanner.hasNext(BIND)) 
      {
        String variableName = scanner.matcher().group(1);

        pattern.setName(variableName);
        symbolTable.set(variableName, pattern);

        scanner.next(); 
      }
      else if(scanner.hasNext(BLOCK_PARAMS_PAREN))
      {
        scanner.next();
        int index = 0;
        while(scanner.hasNext(BLOCK_NAME))
        {
          String blockName = scanner.next();
          BlockNode block = new BlockNode(blockName);

          rule.getBlockParameters().add(block);

          index++;
        }
        if(!scanner.hasNext(BLOCK_PARAMS_PAREN))
        {
          throw new SimpleParseException("Missing block paren", this);
        }
      }

      nextLine();
      
    }
    else
    {
      throw new SimpleParseException("rule must start with valid node label", this);
    }
  }

  private void parseEdges() throws ParseException, IOException
  {
    while(parseEdge()){};
  }

  private boolean parseEdge() throws ParseException, IOException
  {
    scanner = new Scanner(line);
    
    if(scanner.hasNext(EDGE))
    {
      scanner.next();

      int edgeOffset = scanner.matcher().start();

      Node node = offsetTable.getNode(edgeOffset - this.defaultIndentation);
      if(node == null)
      {
        // if edgeOffset is 0 it might be the start of the replacement part
        if(edgeOffset > 0)
          throw new IllegalIndentationException(edgeOffset, this);
        return false;
      }

      
      String edgeLabel = scanner.matcher().group(2);
      String forkGroup = scanner.matcher().group(1);
      boolean forking = !(forkGroup == null || forkGroup.isEmpty());
      
      //Edge edge = new Edge(edgeLabel, indent);
      //Node  

      boolean hasNextNodeLabel = scanner.hasNext(NODE_LABEL);
      boolean hasNextBlockReference = false;
      
      if(!hasNextNodeLabel)
        hasNextBlockReference = scanner.hasNext(BLOCK_REFERENCE);
      
      Node edgeNode;

      if(hasNextNodeLabel || hasNextBlockReference)
      {
        if(hasNextNodeLabel)
        {
          String nodeLabel = scanner.next();

          edgeNode = new Node(nodeLabel(nodeLabel));
          offsetTable.set(scanner.matcher().start(), edgeNode);
        }
        else 
        {
          scanner.next();

          String blockName = scanner.matcher().group(1);
          BlockNode block = rule.findBlockParameterByName(blockName);
          
          if(block == null)
            throw new SimpleParseException(String.format("No such block: '%s' (None of %s)", blockName, rule.getBlockParameterNames()), this);
            
          offsetTable.set(scanner.matcher().start(), block);
          edgeNode = (Node) block;
        }
        
        if(scanner.hasNext(BIND)) 
        {
          String variableName = scanner.matcher().group(1);
          
          edgeNode.setName(variableName);
          symbolTable.set(variableName, edgeNode);

          scanner.next(); 
        }
        
        if(scanner.hasNext(BLOCK_ARGS_PAREN))
        {
          scanner.next();
          
          int index = 0;
          boolean validBlockArgs = true;
                  
          while(validBlockArgs)
          {
            if(scanner.hasNext(BLOCK_NAME))
            {
              String blockLabel = scanner.next();
              String[] names = splitLabel(blockLabel);

              UnparameterizedRuleReference ruleReference = new UnparameterizedRuleReference(names[0] == null ? this.module.getName() : names[0], names[1]);
              BlockArgument blockArgument = new BlockArgument(index, ruleReference);

              edgeNode.getBlockArguments().add(blockArgument);
            }
            else if(scanner.hasNext(BLOCK_REFERENCE))
            {
              scanner.next();
              String blockName = scanner.matcher().group(1);

              BlockNode block = rule.findBlockParameterByName(blockName);
              edgeNode.getBlockArguments().add(new BlockArgument(index, block));
            }
            else
            {
              validBlockArgs = false;
            }
            index++;
          }
          if(!scanner.hasNext(BLOCK_ARGS_PAREN))
          {
            throw new SimpleParseException("Missing block paren", this);
          }
        }

        edgeNode.setForking(forking);

      }
      else if(scanner.hasNext(NODE_REFERENCE))
      {
        String symbol = scanner.matcher().group(1);

        edgeNode = symbolTable.get(symbol);

        scanner.next();
      }
      else 
      {
        throw new SimpleParseException("expected node label, reference or block", this);
      }


      Edge edge = new Edge(edgeLabel, edgeNode);

      rule.getNodes().add(edgeNode);
      
      node.getIncoming().add(edge);
      edgeNode.getOutgoing().add(new Edge(edgeLabel, node));

      nextLine();
    }
    else
    {
      //throw new IllegalEdgeException(line, this);
      return false;
    }
    return true;
  }

  private void parseReplacement() throws ParseException, IOException
  {
    offsetTable = new OffsetTable();

    try {
      parseReplacementSink();
    }
    catch(EOFException e)
    {
      rule.setReplacement(replacement);
      rule.getNodes().add(replacement);

      throw e;
    }


    rule.setReplacement(replacement);
    rule.getNodes().add(replacement);

    parseEdges();
  }

  private void parseReplacementSink() throws ParseException, IOException
  {
    scanner = new Scanner(line, false);
    if(scanner.hasNext(TO_KEYWORD))
    {
      scanner.next();

      boolean hasNextNodeLabel = scanner.hasNext(NODE_LABEL);
      boolean hasNextBlockReference = false;
      
      if(!hasNextNodeLabel)
      {
        hasNextBlockReference = scanner.hasNext(BLOCK_REFERENCE);
      }
      
      if(hasNextNodeLabel || hasNextBlockReference)
      {
        if(hasNextNodeLabel)
        {
          String nodeLabel = scanner.next();
          replacement = new Node(nodeLabel(nodeLabel));
          offsetTable.set(scanner.matcher().start(), replacement);

        }
        else 
        {
          scanner.next();
          String blockName = scanner.matcher().group(1);
          BlockNode block = rule.findBlockParameterByName(blockName);
          offsetTable.set(scanner.matcher().start(), block);
          replacement = (Node) block;
        }
        
        if(scanner.hasNext(BIND)) 
        {
          scanner.next(); 
          String variableName = scanner.matcher().group(1);
          
          replacement.setName(variableName);
          symbolTable.set(variableName, replacement);
        }
        
        if(scanner.hasNext(BLOCK_ARGS_PAREN))
        {
          scanner.next();
          
          int index = 0;
          boolean validBlockArgs = true;
                  
          while(validBlockArgs)
          {
            if(scanner.hasNext(BLOCK_NAME))
            {
              String blockLabel = scanner.next();

              String[] names = splitLabel(blockLabel);
              UnparameterizedRuleReference ruleReference = new UnparameterizedRuleReference(names[0] == null ? this.module.getName() : names[0], names[1]);

              replacement.getBlockArguments().add(new BlockArgument(index, ruleReference));
            }
            else if(scanner.hasNext(BLOCK_REFERENCE))
            {
              scanner.next();
              String blockName = scanner.matcher().group(1);

              BlockNode block = rule.findBlockParameterByName(blockName);
              replacement.getBlockArguments().add(new BlockArgument(index, block));
            }
            else
            {
              validBlockArgs = false;
            }
            index++;
          }
          if(!scanner.hasNext(BLOCK_ARGS_PAREN))
          {
            throw new SimpleParseException("Missing block paren", this);
          }
        }

        if(scanner.hasNext(FORK))
        {
          scanner.next();
          replacement.setForking(true);
        }
      }
      else if(scanner.hasNext(NODE_REFERENCE))
      {
        String symbol = scanner.matcher().group(1);

        replacement = symbolTable.get(symbol);
      }
      else 
      {
        throw new SimpleParseException("expected node label, reference or block", this);
      }
      nextLine();
    }
    else
    {
      throw new SimpleParseException("invalid replacement start", this);
    }
  }

  private String[] splitLabel(String label)
  {
    String names[] = Labels.split(label);

    String moduleName = names[0];
    
    if(moduleName != null)
    {
      if(importTable.containsKey(moduleName))
      {
        names[0] = importTable.get(moduleName);
      }
    }
    return names;
  }
  
  private String nodeLabel(String label)
  {
    String[] names = splitLabel(label);
    return names[0] != null ? (names[0] + '.' + names[1]) : names[1];
  }
  
  private void nextLine() throws IOException
  {
    readNextLine();
    if(line == null)
      throw new EOFException();

    skipEmptyLinesAndComments();
  }

  private void skipEmptyLinesAndComments() throws IOException
  {
    if(line.trim().isEmpty() || line.startsWith(COMMENT_PREFIX) || this.inMultiLineComment)
    {
      if(line.startsWith(MULTI_LINE_COMMENT_PREFIX))
      {
        this.inMultiLineComment = !this.inMultiLineComment;
      }
      nextLine();
    }
  }
  
  private void readNextLine() throws IOException
  {
    line = reader.readLine();
  }

  private void searchAndFlag(Node node, Node.Flag flag)
  {
    Set<Node.Flag> flags = node.getFlags();

    if(flags.contains(flag))
      return;
    else
    {
      flags.add(flag);
      for(Edge edge: node.getIncoming())
      {
        Node incomingNode = edge.getNode();
        searchAndFlag(incomingNode, flag);
      }
    }
  }

  private void postProcess()
  {
    for(Rule rule: ast.getModule().getRules())
    {
      for(Node node: rule.getNodes())
      {
        Collections.sort(node.getIncoming(), EDGE_COMPARATOR);
        Collections.sort(node.getOutgoing(), EDGE_COMPARATOR);
      }

      for(Node node: rule.getNodes())
      {
        node.getFlags().clear();
      } 
      searchAndFlag(rule.getPattern(), Node.Flag.PATTERN_REACHABLE);

      if(rule.getReplacement() instanceof Node)
        searchAndFlag((Node)rule.getReplacement(), Node.Flag.REPLACEMENT_REACHABLE);
    }
  }

}

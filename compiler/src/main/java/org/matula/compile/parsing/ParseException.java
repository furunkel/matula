package org.matula.compile.parsing;

import java.io.File;

public class ParseException extends Exception
{
  private final File file;
  private final int lineNumber;
  private final int columnNumber;
  
  public File getFile()
  {
    return this.file;
  }

  public int getLineNumber()
  {
    return this.lineNumber;
  }

  public int getColumnNumber()
  {
    return this.columnNumber;
  }
  
  public ParseException(File file, int lineNumber, int columnNumber)
  {
    this.file = file;
    this.lineNumber = lineNumber;
    this.columnNumber = columnNumber;
  }

  public ParseException(Parser parser)
  {
    this(parser.getFile(), parser.getLineNumber(), parser.getColumnNumber());
  }

  protected String getMessage(String message)
  {          
    return String.format("%s:%d:%d %s", getFile().getPath(), getLineNumber(), getColumnNumber(), message);
  }
}

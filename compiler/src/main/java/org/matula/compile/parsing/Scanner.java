package org.matula.compile.parsing;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Scanner 
{
  private final char WHITESPACE = ' ';
  private final String source;

  private int offset = 0;
  private Matcher matcher;

  public Scanner(String source, boolean skipWhitespace)
  {
    this.source = source;

    if(skipWhitespace)
    {
      skipWhitespace();
    }
  }

  public Scanner(String source)
  {
    this(source, true);
  }

  public boolean hasNext(String pattern)
  {
    return hasNext(Pattern.compile(pattern));
  }

  public boolean hasNext(Pattern pattern)
  {
    matcher = pattern.matcher(source);
    matcher.region(offset, source.length());
    
    if(matcher.find() /*&& (matcher.end(0) >= source.length() || source.charAt(matcher.end(0)) == SPACE)*/)
    {
      return true;
    }

    return false;
  }

  public String next()
  {
    offset = Math.min(matcher.end(0), source.length() - 1);
    skipWhitespace();
    return matcher.group(0);
  }

  public int offset()
  {
    return offset;
  }

  public Matcher matcher()
  {
    return matcher;
  }

  private void skipWhitespace()
  {
    boolean isWhitespace = source.charAt(offset) == WHITESPACE;
    while(isWhitespace && offset < (source.length() - 1))
    {
      offset++;
      isWhitespace = source.charAt(offset) == WHITESPACE;
    }
  }
}

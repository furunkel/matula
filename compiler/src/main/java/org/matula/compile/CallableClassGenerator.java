package org.matula.compile;

import java.util.List;
import org.matula.compile.ast.Module;
import org.matula.compile.ast.RuleReference;
import org.matula.compile.ast.UnparameterizedRuleReference;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public final class CallableClassGenerator implements Opcodes
{
  private ClassWriter classWriter;
  private String      className;
 
  private static final String BLOCK_THIS_FIELD_NAME = "blockThis";
  
  public CallableClassGenerator(Module module, RuleReference ruleReference, int blockArity, boolean isBlock) throws Exception
  {
    this.classWriter = new ClassWriter(classWriter.COMPUTE_FRAMES | ClassWriter.COMPUTE_MAXS);

    className = className(module, ruleReference);
    String outerClassName = module.getInternalName();

    classWriter.visit(V1_7,
                      ACC_PUBLIC | ACC_SUPER,
                      className,
                      "Ljava/lang/Object;Ljava/util/concurrent/Callable<Lmatula/lang/Node;>;",
                      "java/lang/Object",
                      new String[] { "java/util/concurrent/Callable" });

    classWriter.visitOuterClass(outerClassName, null, null);

    visitConstructor(ruleReference, blockArity, isBlock);
    visitFields(ruleReference, blockArity, isBlock);
    
    visitCallMethod(ruleReference, blockArity, isBlock);
  }
  
  private String blockFieldName(int index)
  {
    return String.format("block_%d", index);
  }
  
  private void visitFields(RuleReference ruleReference, int blockArity, boolean isBlock)
  {
    if(isBlock)
    {
      this.classWriter.visitField(ACC_PRIVATE + ACC_FINAL, BLOCK_THIS_FIELD_NAME,
                                  Constants.BLOCK_TYPE_DESCRIPTOR, null, null);
    }
    
    for(String parameterName: ruleReference.getParameterNames())
    {
     this.classWriter.visitField(ACC_PRIVATE + ACC_FINAL, parameterName,
                                 Constants.NODE_TYPE_DESCRIPTOR, null, null);
    }

    for(int i = 0; i < blockArity; i++)
    {
     this.classWriter.visitField(ACC_PRIVATE + ACC_FINAL, blockFieldName(i),
                                 Constants.BLOCK_TYPE_DESCRIPTOR, null, null);
    }
  }
  
  private void visitConstructor(RuleReference ruleReference, int blockArity, boolean isBlock)
  {
    String descriptor = Util.methodDescriptor(ruleReference.arity(), blockArity, "V", isBlock);
    MethodVisitor mv = classWriter.visitMethod(ACC_PUBLIC, "<init>", descriptor, null, null);
    mv.visitCode();
    
    mv.visitVarInsn(ALOAD, 0);
    mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V");


    if(isBlock)
    {
      mv.visitVarInsn(ALOAD, 0);
      mv.visitVarInsn(ALOAD, 1);
      mv.visitFieldInsn(PUTFIELD, this.className, BLOCK_THIS_FIELD_NAME, Constants.BLOCK_TYPE_DESCRIPTOR);
    }
    
    int index = isBlock ? 2 : 1;
    for(String parameterName: ruleReference.getParameterNames())
    {
      mv.visitVarInsn(ALOAD, 0);
      mv.visitVarInsn(ALOAD, index);
      mv.visitFieldInsn(PUTFIELD, this.className, parameterName, Constants.NODE_TYPE_DESCRIPTOR);

     index++;
    }

    for(int i = 0; i < blockArity; i++)
    {
      mv.visitVarInsn(ALOAD, 0);
      mv.visitVarInsn(ALOAD, 1 + ruleReference.arity() + index);
      mv.visitFieldInsn(PUTFIELD, this.className, blockFieldName(i), Constants.BLOCK_TYPE_DESCRIPTOR);
    }
    
    mv.visitInsn(RETURN);
    
    mv.visitMaxs(0, 0);
    mv.visitEnd();
  }

  private void visitCallMethod(RuleReference ruleReference, int blockArity, boolean isBlock) throws Exception
  {
    MethodVisitor mv = this.classWriter.visitMethod(ACC_PUBLIC, "call", String.format("()%s", Constants.NODE_TYPE_DESCRIPTOR),
                        null, new String[] { "java/lang/Exception" });
    mv.visitCode();

    if(isBlock)
    {
      mv.visitVarInsn(ALOAD, 0);
      mv.visitFieldInsn(GETFIELD, this.className, BLOCK_THIS_FIELD_NAME, Constants.BLOCK_TYPE_DESCRIPTOR);
    }
    
    for(String parameterName: ruleReference.getParameterNames())
    {
      mv.visitVarInsn(ALOAD, 0);
      mv.visitFieldInsn(GETFIELD, this.className, parameterName, Constants.NODE_TYPE_DESCRIPTOR);
    }

    for(int i = 0; i < blockArity; i++)
    {
      mv.visitVarInsn(ALOAD, 0);
      mv.visitFieldInsn(GETFIELD, this.className, blockFieldName(i), Constants.BLOCK_TYPE_DESCRIPTOR);
    }
    
    if(isBlock)
    {
      mv.visitMethodInsn(INVOKEVIRTUAL,
                         Constants.BLOCK_INTERNAL_NAME,
                         "call",
                         ruleReference.getDescriptor(blockArity));
    }
    else
    {
      mv.visitMethodInsn(INVOKESTATIC,
                         ruleReference.getInternalModuleName(),
                         ruleReference.getInternalRuleName(false),
                         ruleReference.getDescriptor(blockArity));
    }

    mv.visitInsn(ARETURN);

    mv.visitMaxs(0, 0);
    mv.visitEnd();
    
    {
      mv = this.classWriter.visitMethod(ACC_PUBLIC + ACC_BRIDGE + ACC_SYNTHETIC, "call",
                                        "()Ljava/lang/Object;", null, new String[] { "java/lang/Exception" });
      mv.visitCode();
      mv.visitVarInsn(ALOAD, 0);
      mv.visitMethodInsn(INVOKEVIRTUAL, this.className, "call", String.format("()%s", Constants.NODE_TYPE_DESCRIPTOR));
      mv.visitInsn(ARETURN);
      mv.visitMaxs(0, 0);
      mv.visitEnd();
    }
  }

  private static String innerClassName(RuleReference ruleReference)
  {
    return String.format("Callable_%s", ruleReference.getInternalRuleName(false)); 
  }
  
  public static String className(Module module, RuleReference ruleReference)
  {
    return String.format("%s$%s", module.getInternalName(), innerClassName(ruleReference));
  }

  public String getClassName()
  {
    return className;
  }

  public void visitEnd()
  {
    this.classWriter.visitEnd();
  }

  public byte[] toByteArray()
  {
    return this.classWriter.toByteArray();
  }
}

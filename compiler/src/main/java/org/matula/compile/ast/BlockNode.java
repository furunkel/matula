package org.matula.compile.ast;

import java.util.Collection;

public class BlockNode extends Node implements BlockArgument.Interface
{
  public BlockNode(String name)
  {
    super(name);
  }
  
  public String getName()
  {
    return getLabel();
  }

  public boolean isBlock()
  {
    return true;
  }

  public int getArity()
  {
    return getIncoming().size();
  }
}

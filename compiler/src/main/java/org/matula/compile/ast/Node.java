package org.matula.compile.ast;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.matula.compile.Labels;
import org.matula.compile.Util;

public class Node
{
  public enum Flag {
    PATTERN_REACHABLE,
    REPLACEMENT_REACHABLE
  }

  public static final Pattern STRING_PATTERN    = Pattern.compile("^\"(.*)");
  public static final Pattern CHARACTER_PATTERN = Pattern.compile("^\'.");

  public static final String WILDCARD_LABEL = "_";
  

  private final String label;
  private final List<Edge> incoming;
  private final List<Edge> outgoing;
  private final List<BlockArgument> blockArguments;
  private final Set<Flag> flags;
  private String name;
  private RuleReference ruleReference;
  private boolean forking;

  public Node(String label)
  {
    this.label = label;
    this.incoming = new ArrayList<Edge>();
    this.outgoing = new ArrayList<Edge>();
    this.blockArguments = new ArrayList<BlockArgument>();
    this.flags = EnumSet.allOf(Flag.class);
    this.forking = false;
  }

  public String getLabel()
  {
    return this.label;
  }

  public String getName()
  {
    return this.name;
  }

  public RuleReference toRuleReference(Module module)
  {
    // can cache since label is final
    if(this.ruleReference == null)
    {
      String moduleName = module != null ? module.getName() : null;
      String[] names = Labels.split(this.label, moduleName);
    
      if(names[0] == null)
      {
        throw new IllegalArgumentException( "cannot infer module name: label" +
                                            " without module and no fallback module given");
      }
      this.ruleReference = new RuleReference(names[0], names[1], getIncomingLabels());
    }

    return this.ruleReference;
  }

  public boolean isInteger()
  {
    return asInteger() != null;
  }

  public boolean isLong()
  {
    return asLong() != null;
  }

  public boolean isDouble()
  {
    return asDouble() != null;
  }

  public boolean isFloat()
  {
    return asFloat() != null;
  }

  public boolean isWildcard()
  {
    return getLabel().equals(WILDCARD_LABEL);
  }

  public boolean isString()
  {
    return asString() != null;
  }

  public boolean isCharacter()
  {
    return asCharacter() != null;
  }

  public Float asFloat()
  {
    try
    {
      return Float.valueOf(label);
    } catch(NumberFormatException e)
    {
      return null;
    }
  }

  public Double asDouble()
  {
    try
    {
      return Double.valueOf(label);
    } catch(NumberFormatException e)
    {
      return null;
    }
  }

  public Integer asInteger()
  {
    try
    {
      return Integer.valueOf(label);
    } catch(NumberFormatException e)
    {
      return null;
    }
  }

  public Long asLong()
  {
    try
    {
      return Long.valueOf(label);
    } catch(NumberFormatException e)
    {
      return null;
    }
  }

  public Character asCharacter()
  {
    Matcher matcher = CHARACTER_PATTERN.matcher(label);
    if(matcher.find())
    {
      return Character.valueOf(matcher.group(1).charAt(0));
    }
    else
    {
      return null;
    }
  }

  public String asString()
  {
    Matcher matcher = STRING_PATTERN.matcher(label);
    if(matcher.find())
    {
      return matcher.group(1);
    }
    else
    {
      return null;
    }
  }

  public int arity()
  {
    return this.incoming.size();
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public List<Edge> getIncoming()
  {
    return this.incoming;
  }

  public List<String> getIncomingLabels()
  {
    List<String> labels = new ArrayList<String>();
    for(Edge edge: this.incoming)
    {
      labels.add(edge.getLabel());
    }
    return labels;
  }

  public List<Edge> getOutgoing()
  {
    return this.outgoing;
  }

  public List<BlockArgument> getBlockArguments()
  {
    return this.blockArguments;
  }

  public int blockArity()
  {
    return this.blockArguments.size();
  }
  
  public Set<Flag> getFlags()
  {
    return flags;
  }

  public boolean isIntersection()
  {
    if(flags.contains(Node.Flag.PATTERN_REACHABLE) &&
       flags.contains(Node.Flag.REPLACEMENT_REACHABLE))
    {
      boolean intersection = true;

      for(Edge edge: getOutgoing())
      {
        Node outgoingNode = edge.getNode();
        Set<Flag> outgoingFlags = outgoingNode.getFlags();

        if(outgoingFlags.contains(Node.Flag.PATTERN_REACHABLE) &&
           outgoingFlags.contains(Node.Flag.REPLACEMENT_REACHABLE))
        {
          intersection = false;
          break;
        }
      }
      return intersection;
    }
    else
    {
      return false;
    }
  }

  public boolean isBlock()
  {
    return false; 
  }

  public static int[] blockArities(Collection<Node> nodes)
  {
    int[] blockArities = new int[nodes.size()];
    int i = 0;
    
    for(Node node: nodes)
    {
      blockArities[i] = node.getBlockArguments().size();
      i++;
    }
    return blockArities;
  }

  public void setForking(boolean forking)
  {
    this.forking = forking;
  }

  public boolean isForking()
  {
    return this.forking;
  }
  

  
}

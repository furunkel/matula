package org.matula.compile.ast;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import org.matula.compile.Constants;

import org.matula.compile.ModuleLoader;
import org.matula.compile.Util;
import org.matula.compile.parsing.ParseException;

public final class RuleReference extends UnparameterizedRuleReference implements Constants
{
  private static final int MAX_BLOCKS = 10;
          
  public static final class BlockParameters
  {
    private final List<String> paramterNames;
    private final int blockArity;
 
    public BlockParameters(List<String> parameterNames, int blockArity)
    {
      this.paramterNames = parameterNames;
      this.blockArity = blockArity;
    }

    public List<String> getParamterNames()
    {
      return this.paramterNames;
    }

    public int arity()
    {
      return this.paramterNames.size();
    }
    
    public int blockArity()
    {
      return this.blockArity;
    }
  }
          
  private final List<String> parameterNames;
  private Boolean isIdentity = null;
 
  public RuleReference(UnparameterizedRuleReference ruleReference, List<String> parameterNames)
  {
    super(ruleReference.getModuleName(), ruleReference.getRuleName());
    this.parameterNames = parameterNames;
  }
  
  public RuleReference(String moduleName, String ruleName, List<String> parameterNames)
  {
    super(moduleName, ruleName);
    this.parameterNames = Collections.unmodifiableList(parameterNames);
  }

  public List<String> getParameterNames()
  {
    return this.parameterNames;
  }

  public int arity()
  {
    return this.parameterNames.size();
  }

  public int blockArity()
  {
    //FIXME: The same story, try getting from AST, on failure reflect
    return -1;
  }

  public String getInternalRuleName(boolean isIdentity)
  {
    StringBuilder nameBuilder = new StringBuilder(Util.toLowerCamelCase(this.ruleName));
    
    if(!isIdentity)
    {
      for(String parameterName: this.parameterNames)
      {
        nameBuilder.append("_");
        nameBuilder.append(Util.toLowerCamelCase(parameterName));
      }
    }

    return nameBuilder.toString();
  }
  
  public String getInternalRuleName() throws Exception
  {
    return getInternalRuleName(this.isIdentity());
  }
          
  private Method findMethodByName(String name) throws ClassNotFoundException
  {
    Class<?> moduleClass = Class.forName(this.moduleName);

    // Slow for large modules, alternatively try getMethod directly
    // with increasing number of block arguments
    Method[] methods = moduleClass.getMethods();
    Method method = null;
    for(Method m: methods)
    {
      if(m.getName().equals(name))
      {
        method = m;
      }
    }
    return method;
    //moduleClass.getMethod(getInternalRuleName(), parameterTypes(i));
  }

  private Class<?>[] parameterTypes(int blockArity)
  {
    int arity = this.parameterNames.size();
    Class<?>[] paramTypes = new Class<?>[arity + blockArity];
    for(int i = 0; i < arity; i++)
    {
      paramTypes[i] = matula.lang.Node.class;
    }

    for(int i = arity; i < blockArity; i++)
    {
      paramTypes[i] = matula.lang.Block.class;
    }

    return paramTypes;
  }

  
  public BlockParameters getBlockParameters(int index) throws IOException, ParseException, ClassNotFoundException, NoSuchMethodException
  {
    try
    {

      Module module = ModuleLoader.INSTANCE.load(this.moduleName);
      List<Rule> rules = module.findRulesByName(this.ruleName);

      int blockArity = -1;
      List<String> blockParameterNames = null;

      for(Rule rule: rules)
      {
        if(rule.getParameterNames().equals(this.parameterNames))
        {
          if(rule.getBlockParameterNames().size() > index)
          {
            BlockNode b = rule.getBlockParameters().get(index);

            // check if block is actually used
            if(rule.getNodes().contains(b))
            {
              
              List<String> p = new ArrayList<String>();
            
              for(Edge edge: b.getIncoming())
              {
                p.add(edge.getLabel());
              }

              if(blockParameterNames != null && !blockParameterNames.equals(p))
              {
                throw new RuntimeException(String.format("inconsistent block parameters (%s vs. %s)", blockParameterNames, p)); 
              }
              else if(blockArity != -1 && blockArity != b.blockArity())
              {
                throw new RuntimeException(String.format("inconsistent block arities (%d vs. %d)", blockArity, b.blockArity())); 
              }
              else
              {
                blockParameterNames = p;
                blockArity = b.blockArity();
              }  
            }
          }
        }
      }

      if(blockParameterNames == null)
      {
        throw new NoSuchElementException();
      }
      
      return new BlockParameters(blockParameterNames, blockArity);

    } catch (FileNotFoundException | NoSuchElementException ex)
    {
      // Try loading .class file
      List<String> blockParameterNames = new ArrayList<String>();

      Method method = findMethodByName(getInternalRuleName(false));
        
      Annotation annotation = method.getAnnotation(matula.annotations.Blocks.class);
      matula.annotations.Blocks blocks = (matula.annotations.Blocks)annotation;
        
      matula.annotations.Block block = blocks.value()[index];

      for(String parameter: block.parameters())
      {
        blockParameterNames.add(parameter);
      }
      return new BlockParameters(blockParameterNames, block.blockArity());
    }
  }

  public boolean isIdentity() throws FileNotFoundException, IOException, ParseException, ClassNotFoundException
  {
    if(this.isIdentity != null)
      return this.isIdentity;
   
    Boolean isIdentity = Boolean.FALSE;

    try
    {
      Module module = ModuleLoader.INSTANCE.load(this.moduleName);
      List<Rule> rules = module.findRulesByName(this.ruleName);

      /*
      Rule rule = null;
      for(Rule r: rules)
      {
        if(r.getParameterNames().equals(this.parameterNames))
          rule = r;
      }
      */

      if(rules.size() != 1)
      {
        for(Rule r: rules)
        {
          if(r.isIdentity())
          {
            //FIXME: Throw something better than RuntimeException
            // and find a better name for "overloading"
            throw new RuntimeException("identity rule overloading: " + r.getName());
          }
        }
      }
      isIdentity = rules.get(0).isIdentity();

    } catch (FileNotFoundException | NoSuchElementException ex)
    {
      Method method = findMethodByName(getRuleName());
      // no such method means no
      if(method == null)
      {
        isIdentity = Boolean.FALSE;
      }
      else
      {
        Annotation annotation = method.getAnnotation(matula.annotations.IdentityRule.class);
        isIdentity = annotation != null;
      }
    }
    this.isIdentity = isIdentity;

    return isIdentity;
  }

  /*public boolean isLinear() throws ClassNotFoundException, NoSuchMethodException
  {
    Class<?> moduleClass = Class.forName(Util.internalizeModuleName_(Util.className(getLabel())));
    Class<?>[] paramTypes = parameterTypes();
    Method ruleMethod = moduleClass.getMethod(Util.methodName(getLabel()), paramTypes);
    
    Annotation annotation = ruleMethod.getAnnotation(LinearRule.class);

    return annotation != null;
  }*/
  
  public String getDescriptor(int blockArity)
  {
    return Util.methodDescriptor(arity(), blockArity);
  }
}

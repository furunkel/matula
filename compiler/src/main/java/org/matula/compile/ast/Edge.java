package org.matula.compile.ast;

public final class Edge
{
  private String label;
  private final Node node;

  public Edge(String label, Node node)
  {
    this.label = label;
    this.node = node;
  }

  public String getLabel()
  {
    return this.label;
  }

  public void setLabel(String label)
  {
    this.label = label;
  }

  public Node getNode()
  {
    return this.node;
  }
}

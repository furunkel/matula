package org.matula.compile.ast;

import org.matula.compile.Constants;
import org.matula.compile.Util;

public class UnparameterizedRuleReference implements BlockArgument.Interface
{
  protected final String ruleName; 
  protected final String moduleName;
  
  public UnparameterizedRuleReference(String moduleName, String ruleName)
  {
    if(moduleName == null)
      throw new NullPointerException("module name cannot be null");

    if(moduleName.indexOf(Constants.INTERNAL_MODULE_NAME_SEPARATOR) != -1)
      throw new IllegalArgumentException(String.format("module name must not be in internal form (%s)", moduleName));

    if(ruleName == null)
      throw new NullPointerException("rule name cannot be null");

    this.ruleName = ruleName;
    this.moduleName = moduleName;
  }
  
  public String getRuleName()
  {
    return ruleName;
  }

  public String getModuleName()
  {
    return moduleName;
  }

  public String getInternalModuleName()
  {
    return Util.internalizeModuleName(moduleName);
  }
}

package org.matula.compile.ast;

import java.util.List;
import java.util.ArrayList;

public final class Ast
{
  private Module module = null;

  public Ast()
  {
  }

  public void setModule(Module module)
  {
    this.module = module; 
  }

  public Module getModule()
  {
    return module;
  }
  
}

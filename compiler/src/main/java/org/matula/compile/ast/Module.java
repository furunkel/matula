package org.matula.compile.ast;

import java.util.List;
import java.util.ArrayList;
import org.matula.compile.Labels;
import org.matula.compile.Util;

public class Module
{
  private String name;
  private final List<Rule> rules;

  public Module()
  {
    this.rules = new ArrayList<Rule>();
  }

  public List<Rule> getRules()
  {
    return this.rules;
  }
  
  public String getName()
  {
    return this.name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getInternalName()
  {
    return Util.internalizeModuleName(name);
  }

  public List<Rule> findRulesByName(String name)
  {
    List<Rule> rules = new ArrayList<Rule>();
    
    for(Rule rule: this.rules)
    {
      if(Labels.ruleName(rule.getName()).equals(name))
      {
        rules.add(rule);
      }
    }
    return rules;
  }
}

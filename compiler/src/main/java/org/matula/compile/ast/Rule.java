package org.matula.compile.ast;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.matula.compile.Util;

public final class Rule
{
  private Node pattern;
  private Node replacement;
  private final List<Node> nodes;
  private final List<BlockNode> blockParameters;

  public static final int MAX_SIZE = 1000;

  public Rule()
  {
    this.nodes = new ArrayList<Node>();
    this.blockParameters = new ArrayList<BlockNode>();
  }

  public Node getPattern()
  {
    return pattern;
  }

  public void setPattern(Node pattern)
  {
    this.pattern = pattern;
    this.pattern.getFlags().add(Node.Flag.PATTERN_REACHABLE);
  }

  public Node getReplacement()
  {
    return this.replacement;
  }

  public void setReplacement(Node replacement)
  {
    this.replacement = replacement;
    this.replacement.getFlags().add(Node.Flag.REPLACEMENT_REACHABLE);
  }

  public List<Node> getNodes()
  {
    return this.nodes;
  }

  public List<BlockNode> getBlockParameters()
  {
    return this.blockParameters;
  }

  public boolean isIdentity()
  {
    return this.pattern == this.replacement;
  }

  public String getName()
  {
    return this.pattern.getLabel();
  }

  public int arity()
  {
    return this.pattern.getIncoming().size();
  }
  
  public int blockArity()
  {
    return blockParameters.size();
  }

  public List<String> getParameterNames()
  {
    return this.pattern.getIncomingLabels(); 
  }
  
  public RuleReference toRuleReference(Module module)
  {
    return this.pattern.toRuleReference(module);
  }

  private int[] path(Node node, Node targetNode, int[] path)
  {
    if(node == targetNode)
      return path;
    else
    {
      List<Edge> incoming = node.getIncoming();
      if(incoming.isEmpty())
        return null;
   
      int[] result = null;
      for(int i = 0; i < incoming.size(); i++)
      {
        Node incomingNode = incoming.get(i).getNode();

        int[] newPath = new int[path.length + 1];
        newPath[path.length] = i;
        System.arraycopy(path, 0, newPath, 0, path.length);

        result = path(incomingNode, targetNode, newPath);
        if(result != null)
          break;
      }
      return result;
    }
  }

  public int[] patternPath(Node node)
  {
    return path(this.pattern, node, new int[]{});
  }

  public List<String> getBlockParameterNames()
  {
    List<String> blockParameterNames = new ArrayList<String>();
    for(BlockNode block: blockParameters)
    {
      blockParameterNames.add(block.getName());
    }
    return blockParameterNames;
  }

  public BlockNode findBlockParameterByName(String name)
  {
    for(BlockNode block: blockParameters)
    {
      if(block.getName().equals(name))
        return block;
    }
    return null;
  }

}

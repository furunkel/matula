package org.matula.compile.ast;

import java.util.ArrayList;
import java.util.List;

public final class BlockArgument
{
  public interface Interface {}

  private final int index;
  private final Interface object;
  
  public BlockArgument(int index, Interface object)
  {
     this.index = index;
     this.object = object;
  }

  public Interface get()
  {
    return this.object;
  }

  public int getIndex()
  {
    return this.index;
  }
}

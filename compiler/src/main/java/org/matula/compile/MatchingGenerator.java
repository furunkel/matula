package org.matula.compile;

import org.matula.compile.ast.Edge;
import org.matula.compile.ast.Module;
import org.matula.compile.ast.Node;
import org.matula.compile.ast.Rule;
import org.matula.compile.ast.RuleReference;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public class MatchingGenerator implements Opcodes
{
  public static void generate(Module module, MethodVisitor mv, Rule rule,  Node node, Label succLabel, Label failLabel, Label popAndFailLabel)
  {
    assert !node.isWildcard();

    if(node.isInteger())
    {
      mv.visitInsn(DUP);

      mv.visitFieldInsn(GETFIELD, "matula/lang/Node", "id", "I");
      mv.visitFieldInsn(GETSTATIC, "matula/lang/Integer", "CLASS_ID", "I");
      mv.visitJumpInsn(IF_ICMPNE, popAndFailLabel);
      
      mv.visitTypeInsn(CHECKCAST, "matula/lang/Integer");
      mv.visitFieldInsn(GETFIELD, "matula/lang/Integer", "value", "I");
      mv.visitLdcInsn((int)node.asInteger());
      mv.visitJumpInsn(IF_ICMPNE, failLabel);
    }
    else if(node.isDouble())
    {
      mv.visitInsn(DUP);
      
      mv.visitFieldInsn(GETFIELD, "matula/lang/Node", "id", "I");
      mv.visitFieldInsn(GETSTATIC, "matula/lang/Double", "CLASS_ID", "I");
      mv.visitJumpInsn(IF_ICMPNE, popAndFailLabel);
      
      mv.visitTypeInsn(CHECKCAST, "matula/lang/Double");
      mv.visitFieldInsn(GETFIELD, "matula/lang/Double", "value", "D");
      mv.visitLdcInsn((double)node.asDouble());
      mv.visitInsn(DCMPL);
      mv.visitJumpInsn(IFNE, failLabel);
    }
    else
    {
      RuleReference ruleReference = node.toRuleReference(module);

      int nonWildcards = 0;
      for(Edge edge: node.getIncoming())
      {
        Node incomingNode = edge.getNode();
        if(!incomingNode.isWildcard())
        {
          nonWildcards++;
        }
      }

      if(nonWildcards > 0)
        mv.visitInsn(DUP);

      mv.visitFieldInsn(GETFIELD, "matula/lang/Node", "id", "I");
      mv.visitFieldInsn(GETSTATIC, NodeClassGenerator.className(ruleReference), "CLASS_ID", "I");

      if(nonWildcards > 0)
        mv.visitJumpInsn(IF_ICMPNE, popAndFailLabel);
      else
        mv.visitJumpInsn(IF_ICMPNE, failLabel);
    
      Label nextPopAndFailLabel = null;
      if(nonWildcards > 1) 
        nextPopAndFailLabel = new Label();

      int nonWildcardCounter = 0;
      for(Edge edge: node.getIncoming())
      {
        String edgeLabel = edge.getLabel();
        Node incomingNode = edge.getNode();

        if(!incomingNode.isWildcard())
        {
          if(nonWildcardCounter < nonWildcards - 1)
            mv.visitInsn(DUP);

          String className = NodeClassGenerator.className(ruleReference);

          // CHECKCAST seems not really necessary
          mv.visitTypeInsn(CHECKCAST, className);

          mv.visitFieldInsn(GETFIELD, className, edgeLabel, "Lmatula/lang/Node;");

          // VERSION A
          // jump a lot to get a clean stack in any case
          if(nonWildcardCounter < nonWildcards - 1)
          {
            generate(module, mv, rule, incomingNode, popAndFailLabel, nextPopAndFailLabel);
          }
          else
          {
            generate(module, mv, rule, incomingNode, failLabel, popAndFailLabel);
          }

          // VERSION B
          // this would leak stack, but not require all these jumps
          // however, ASM cannot infer frames when using this, wonder why  ?
          //generateMatching(mv, incomingNode, popAndFailLabel, popAndFailLabel);
          //
          nonWildcardCounter++;
        }
      }

      // VERSION A
      mv.visitJumpInsn(GOTO, succLabel);
      if(nonWildcards > 1)
      {
        mv.visitLabel(nextPopAndFailLabel);
        mv.visitInsn(POP);
        mv.visitJumpInsn(GOTO, popAndFailLabel);
      }
    }
  }

  public static void generate(Module module, MethodVisitor mv, Rule rule,  Node node, Label failLabel, Label popAndFailLabel)
  {
    Label succLabel = new Label();

    generate(module, mv, rule, node, succLabel, failLabel, popAndFailLabel);

    mv.visitLabel(succLabel);
  }
  
}

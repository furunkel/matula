package org.matula.compile;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.matula.compile.ast.BlockArgument;
import org.matula.compile.ast.Edge;
import org.matula.compile.ast.Module;
import org.matula.compile.ast.Node;
import org.matula.compile.ast.Rule;
import org.matula.compile.ast.RuleReference;
import org.matula.compile.ast.UnparameterizedRuleReference;
import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.util.CheckClassAdapter;


public final class ClassGenerator implements Opcodes
{

  protected Callback callback;
  protected String className;
  protected ClassWriter classWriter;
  private Set<String> blockClassNames;
  private Set<String> callableClassNames;
  
  private MethodVisitor clinitVisitor;
  private final Module module;


  public enum ThreadPoolType
  {
    CACHED,
    FIXED
  }

  public interface Callback
  {
    void call(String name, byte[] bytecode);
  }

  public ClassGenerator(Module module, Callback cb)
  {

    this.className = module.getInternalName();
    this.classWriter = new ClassWriter(ClassWriter.COMPUTE_FRAMES | ClassWriter.COMPUTE_MAXS);

    this.classWriter.visit(V1_7,
                           ACC_PUBLIC | ACC_SUPER,
                           this.className,
                           null, 
                           "java/lang/Object",
                           null);

    this.module = module;
    this.callback = cb;
    this.blockClassNames = new HashSet<String>();
    this.callableClassNames = new HashSet<String>();

    this.clinitVisitor = classWriter.visitMethod(ACC_STATIC, "<clinit>", "()V", null, null);
    this.clinitVisitor.visitCode();

    MethodVisitor mv = classWriter.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
    mv.visitCode();
    mv.visitVarInsn(ALOAD, 0);
    mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V");
    mv.visitInsn(RETURN);
    mv.visitMaxs(0, 0);
    mv.visitEnd();
  }
  
  public void visitEnd()
  {
    clinitVisitor.visitInsn(RETURN);
    clinitVisitor.visitMaxs(0, 0);
    clinitVisitor.visitEnd();
    classWriter.visitEnd();
  }

  public byte[] toByteArray()
  {
    return classWriter.toByteArray();
  }

  protected MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions)
  {
    return classWriter.visitMethod(access, name, desc, signature, exceptions);
  }

  private void matchAndGenerateChain(MethodVisitor mv, List<Rule> rules, int arity, int blockArity, boolean isInMain) throws Exception
  {
    Label[] labels = new Label[rules.size() + 1]; 
    for(int i = 1; i < rules.size(); i++)
    {
      labels[i] = new Label();
    }

    Label finalLabel = new Label();
    labels[rules.size()] = finalLabel;

    Label[] popLabels = new Label[rules.size() + 1]; 
    for(int i = 1; i < rules.size(); i++)
    {
      popLabels[i] = new Label();
    }

    Label finalPopLabel = new Label();
    popLabels[rules.size()] = finalPopLabel;

    int index = 0;
    for(Rule rule: rules)
    {
      if(index > 0)
      {
        mv.visitLabel(popLabels[index]);
        mv.visitInsn(POP);

        mv.visitLabel(labels[index]);
      }
     
      Node patternNode = rule.getPattern(); 
      Node replacementNode = rule.getReplacement();

//      visitEdges(patternNode);
//      int edgeIndex = 0;
//      for(Edge edge: patternNode.getIncoming())
//      {
//        String edgeLabel = edge.getLabel();
//
//        mv.visitVarInsn(ILOAD, 2 * edgeIndex);
//
//        /*FIXME: check if we really have no more than Short.MAX_VALUE edge ids !!!*/
//        mv.visitFieldInsn(GETSTATIC, this.className, Util.toConstantCase(edgeLabel), "I");
//        mv.visitJumpInsn(IF_ICMPNE, labels[index + 1]);
//
//        edgeIndex++;
//      }
 
      int edgeIndex = 0;
      for(Edge edge: patternNode.getIncoming())
      {
        String edgeLabel = edge.getLabel();
        Node node = edge.getNode();

        if(!node.isWildcard())
        {
          //mv.visitVarInsn(ALOAD, 2 * edgeIndex + 1);
          mv.visitVarInsn(ALOAD, edgeIndex);
          MatchingGenerator.generate(this.module, mv, rule, node, labels[index + 1], popLabels[index + 1]);
        }
        edgeIndex++;
      } 

      Map<Node, Integer> varIndexMap = new IdentityHashMap<Node, Integer>();
      CallGraphGenerator.generate(this.module, mv, rule, replacementNode, varIndexMap, new int[]{arity + blockArity});

      if(isInMain)
      {

        mv.visitFieldInsn(GETSTATIC, "matula/runtime/Runtime", Constants.DEFAULT_EXECUTOR_SERVICE_CONSTANT_NAME, "Ljava/util/concurrent/ExecutorService;");
        mv.visitMethodInsn(INVOKEINTERFACE, "java/util/concurrent/ExecutorService", "shutdown", "()V");
    
       mv.visitInsn(RETURN);
      }
      else
      {
        mv.visitVarInsn(ALOAD, varIndexMap.get(replacementNode));
        mv.visitInsn(ARETURN);
      }

      index++;
    }

    mv.visitLabel(finalPopLabel);
    mv.visitInsn(POP);
    mv.visitLabel(finalLabel);
    
  }

  public static void generateThrowNoMatchException(MethodVisitor mv)
  {
    mv.visitTypeInsn(NEW, "matula/runtime/NoMatchException");
    mv.visitInsn(DUP);
    mv.visitMethodInsn(INVOKESPECIAL, "matula/runtime/NoMatchException", "<init>", "()V");
    mv.visitInsn(ATHROW);
  }

  protected void visitDestructorMethod(List<Rule> rules) throws Exception
  {
    Rule rule = rules.get(0);
    RuleReference ruleReference = rule.toRuleReference(this.module);   
    
    MethodVisitor mv = this.visitMethod(ACC_PUBLIC | ACC_STATIC, ruleReference.getInternalRuleName(),
            ruleReference.getDescriptor(rule.blockArity()), null, null);
   
    mv.visitAnnotation("Lmatula/annotations/Safe;", true).visitEnd();
    
    List<Node> blocks = new ArrayList<Node>();
    

    for(Rule r: rules)
    {
      for(Node node: r.getNodes())
      { 
        if(node.isBlock())
        {
          blocks.add(node);
        }
      }
    }

    if(!blocks.isEmpty())
    {
      AnnotationVisitor av = mv.visitAnnotation("Lmatula/annotations/Blocks;", true);
      AnnotationVisitor blocksVisitor = av.visitArray("blocks");

      for(Node block: blocks)
      {
          AnnotationVisitor blockVisitor = blocksVisitor.visitAnnotation("", "Lmatula/annotations/Block;");
          blockVisitor.visit("blockArity", block.getBlockArguments().size());
          AnnotationVisitor parametersVisitor = blockVisitor.visitArray("parameters");
          for(Edge edge: block.getIncoming())
          {
            parametersVisitor.visit("", edge.getLabel());
          }
          parametersVisitor.visitEnd();
          blockVisitor.visitEnd();
      }
      blocksVisitor.visitEnd();
      av.visitEnd();
    }

    mv.visitCode();
    visitBlockConstants(rules);
    visitCallableClasses(rules);
    
    matchAndGenerateChain(mv, rules, ruleReference.arity(), rule.blockArity(), false);

    generateThrowNoMatchException(mv);

    mv.visitMaxs(0, 0);
    mv.visitEnd();
  }

  protected void visitConstructorMethod(Rule rule)
  {

    RuleReference ruleReference = rule.toRuleReference(this.module);

    String nodeClassName = NodeClassGenerator.className(ruleReference);
    
    MethodVisitor mv = this.visitMethod(ACC_PUBLIC | ACC_STATIC, ruleReference.getInternalRuleName(true),
                                        ruleReference.getDescriptor(rule.blockArity()), null, null);


    {
      AnnotationVisitor av = mv.visitAnnotation("Lmatula/annotations/IdentityRule;", true);
      av.visitEnd();
    }

    mv.visitAnnotation("Lmatula/annotations/Safe;", true).visitEnd();
    
    {
      AnnotationVisitor av = mv.visitAnnotation("Lmatula/annotations/RuleSize;", true);
      av.visit("value", 1);
      av.visitEnd();
    }
    
    mv.visitCode();

    mv.visitTypeInsn(NEW, nodeClassName);
    mv.visitInsn(DUP);

    Node node = rule.getPattern();
   
//    Label failLabel = new Label();
//    Label popAndFailLabel = new Label();
//    Label succLabel = new Label();
//    
//    generateMatching(mv, node, succLabel, failLabel, popAndFailLabel);
//
//    mv.visitLabel(popAndFailLabel);
//    mv.visitInsn(POP);
//    mv.visitLabel(failLabel);
//    generateThrowNoMatchException(mv);
//    mv.visitLabel(succLabel);
    
    int index = 0;
    for(Edge edge: node.getIncoming())
    {
      mv.visitVarInsn(ALOAD, index);

        // Do not reference when just wrapping in data.
        // This way linear nodes are not referenced by data
        // FIXME: Is it really okay to do that ?
        //mv.visitInsn(DUP);
        //mv.visitMethodInsn(INVOKEVIRTUAL, "matula/lang/Node", "reference", "()V");
      index++;
    }
    
    mv.visitMethodInsn(INVOKESPECIAL, nodeClassName, "<init>", NodeClassGenerator.constructorDescriptor(node.getIncoming().size()));

    //visitEdges(node);
  
    mv.visitInsn(ARETURN);
    mv.visitMaxs(0, 0);
    mv.visitEnd();
  }

  public static String blockConstantName(Module module, UnparameterizedRuleReference ruleReference, RuleReference.BlockParameters blockParameters)
  {
    return String.format("BLOCK_%s", BlockClassGenerator.className(module, ruleReference, blockParameters).replace(Constants.INTERNAL_MODULE_NAME_SEPARATOR, '_').replace('$','_').toUpperCase());
  }

  public void visitBlockConstants(List<Rule> rules) throws Exception
  {
    for(Rule rule: rules)
    {
      for(Node node: rule.getNodes())
      {
        if(!node.getBlockArguments().isEmpty())
        {
          RuleReference nodeRuleReference = node.toRuleReference(this.module);

          int index = 0;
          for(BlockArgument blockArgument: node.getBlockArguments())
          {
            if(blockArgument.get() instanceof UnparameterizedRuleReference)
            {
              UnparameterizedRuleReference blockRuleReference = (UnparameterizedRuleReference) blockArgument.get();
              RuleReference.BlockParameters blockParameters = nodeRuleReference.getBlockParameters(index);
              visitBlockConstant(blockRuleReference, blockParameters);
            }
            index++;
          }
        }
      }
    }
  }

  public void visitCallableClasses(List<Rule> rules) throws Exception
  {
    for(Rule rule: rules)
    {
      for(Node node: rule.getNodes())
      {
        if(node.isForking())  
        {
          RuleReference nodeRuleReference = node.toRuleReference(this.module);
          visitCallableClass(nodeRuleReference, node.blockArity(), node.isBlock());
        }
      }
    }

  }

  public void visitBlockConstant(UnparameterizedRuleReference ruleReference, RuleReference.BlockParameters blockParameters) throws Exception
  {
    String blockClassName = BlockClassGenerator.className(this.module, ruleReference, blockParameters);

    String fieldName = blockConstantName(this.module, ruleReference, blockParameters);
    if(blockClassNames.contains(blockClassName))
    {
      return;
    }
    // new ....
    blockClassNames.add(blockClassName);

    visitBlockClass(ruleReference, blockParameters);
    
    this.classWriter.visitField(ACC_PUBLIC | ACC_FINAL | ACC_STATIC,
                                fieldName,
                                "Lmatula/lang/Block;",null, null).visitEnd();
  
    this.clinitVisitor.visitTypeInsn(NEW, blockClassName);
    this.clinitVisitor.visitInsn(DUP);
    this.clinitVisitor.visitMethodInsn(INVOKESPECIAL, blockClassName, "<init>", "()V");
    this.clinitVisitor.visitFieldInsn(PUTSTATIC, this.className, fieldName, "Lmatula/lang/Block;"); 
  }

  public void visitCallableClass(RuleReference ruleReference, int blockArity, boolean isBlock) throws Exception
  {
    String callableClassName = CallableClassGenerator.className(this.module, ruleReference);

    if(callableClassNames.contains(callableClassName))
    {
      return;
    }
    // new ....
    callableClassNames.add(callableClassName);

    this.classWriter.visitInnerClass(callableClassName, null, null, ACC_PUBLIC | ACC_FINAL | ACC_STATIC); 

    CallableClassGenerator callableGenerator = new CallableClassGenerator(this.module, ruleReference, blockArity, isBlock);
    callableGenerator.visitEnd();
    callback.call(callableClassName, callableGenerator.toByteArray());
  }
  
  public String visitBlockClass(UnparameterizedRuleReference ruleReference, RuleReference.BlockParameters blockParameters) throws Exception
  {
    BlockClassGenerator blockGenerator = new BlockClassGenerator(this.module, ruleReference, blockParameters);
    String blockClassName = blockGenerator.getClassName();
    this.classWriter.visitInnerClass(blockClassName, null, null, ACC_PUBLIC | ACC_FINAL | ACC_STATIC); 

    blockGenerator.visitEnd();
    callback.call(blockClassName, blockGenerator.toByteArray());

    return blockClassName;
  }

  public void visitNodeClass(Rule rule)
  {
    Node node = rule.getPattern();
    String nodeClassName = NodeClassGenerator.className(node.toRuleReference(this.module));

    this.classWriter.visitInnerClass(nodeClassName, null, null, ACC_PUBLIC | ACC_FINAL | ACC_STATIC);
    
    NodeClassGenerator nodeGenerator = new NodeClassGenerator(this.module, node);
    nodeGenerator.visitEnd();
    
    callback.call(nodeClassName, nodeGenerator.toByteArray());
  }


  protected boolean isIdentityRuleSet(List<Rule> rules)
  {
    return rules.size() == 1 && rules.get(0).isIdentity();
  }
 
  public void visitMainRuleSet(List<Rule> rules, ThreadPoolType threadPoolType, int nThreads) throws Exception
  {
    final MethodVisitor mv;
    
    String methodName = "main";
    int arity = 0;

    String descriptor = "([Ljava/lang/String;)V";

    mv = this.visitMethod(ACC_PUBLIC | ACC_STATIC, methodName, descriptor, null, null);
    mv.visitCode();

    String threadPoolMethodName = null;
    switch(threadPoolType)
    {
      case CACHED:
        threadPoolMethodName = "newCachedThreadPool";
        break;
      case FIXED:
        threadPoolMethodName = "newFixedThreadPool";
        break;
    }

    if(nThreads > 0)
    {
      mv.visitLdcInsn(nThreads);
    }
    else
    {
      // 5 threads per core by default
      mv.visitMethodInsn(INVOKESTATIC, "java/lang/Runtime", "getRuntime", "()Ljava/lang/Runtime;");
      mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Runtime", "availableProcessors", "()I");
      mv.visitLdcInsn(5);
      mv.visitInsn(IMUL);
    }
    
    mv.visitMethodInsn(INVOKESTATIC, "java/util/concurrent/Executors", threadPoolMethodName, "(I)Ljava/util/concurrent/ExecutorService;");
    mv.visitFieldInsn(PUTSTATIC, "matula/runtime/Runtime", Constants.DEFAULT_EXECUTOR_SERVICE_CONSTANT_NAME, "Ljava/util/concurrent/ExecutorService;");

    visitBlockConstants(rules);
    visitCallableClasses(rules);
    
    matchAndGenerateChain(mv, rules, arity, 0, true);

    generateThrowNoMatchException(mv);


    mv.visitMaxs(0, 0);
    mv.visitEnd();
  }
 
  public void visitRuleSet(List<Rule> rules) throws Exception
  {
    if(rules.isEmpty())
    {
      return;
    }
    
    if(isIdentityRuleSet(rules))
    {
      visitNodeClass(rules.get(0));
      visitConstructorMethod(rules.get(0));
    }
    else
    {
      visitDestructorMethod(rules);
    }
  }
  
  public static void verify(byte[] bytecode)
  {
    PrintWriter pw = new PrintWriter(System.out);
    CheckClassAdapter.verify(new ClassReader(bytecode), true, pw);
  }
}

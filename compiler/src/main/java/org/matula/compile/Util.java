package org.matula.compile;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.matula.compile.ast.Edge;
import org.matula.compile.ast.Node;
import org.matula.compile.ast.RuleReference;

public final class Util implements Constants
{
  
  public static String toLowerCamelCase(String[] parts)
  {
    if(parts.length == 0)
      return "";

    StringBuilder builder = new StringBuilder(parts[0]);
    for(int i = 1; i < parts.length; i++) 
    {
      builder.append(capitalize(parts[i]));
    }
    return builder.toString();
  }
          
  public static String toLowerCamelCase(String string)
  {
    return toLowerCamelCase(string.split("\\-"));
  }

  public static String toUpperCamelCase(String[] parts)
  {
    return capitalize(toLowerCamelCase(parts));
  }

  public static String toUpperCamelCase(String string)
  {
    return capitalize(toLowerCamelCase(string));
  }

  public static String capitalize(String string)
  {
    return Character.toUpperCase(string.charAt(0)) + string.substring(1);
  }

  public static String internalizeModuleName(String name)
  {
    return name.replace(MODULE_NAME_SEPARATOR, INTERNAL_MODULE_NAME_SEPARATOR);
  }

  public static String internalizeRuleName(String name)
  {
    return name.replace(RULE_NAME_SEPARATOR, INTERNAL_RULE_NAME_SEPARATOR);
  }

  public static String canoncialModuleName(String name)
  {
    return name.replace(INTERNAL_MODULE_NAME_SEPARATOR, MODULE_NAME_SEPARATOR);
  }

  public static List<String> incomingEdgeLabels(Node node)
  {
    List<String> edgeLabels = new ArrayList<String>(node.getIncoming().size());

    for(Edge edge: node.getIncoming())
    {
      edgeLabels.add(edge.getLabel());
    }
    return edgeLabels;
  }

  public static String edgeLabelToFieldName(String label)
  {
    return toLowerCamelCase(label);
  }

  public static String methodDescriptor(int arity, int blockArity, String returnTypeDescriptor, boolean firstBlock)
  {
    StringBuilder builder = new StringBuilder("(");
    if(firstBlock)
      builder.append("Lmatula/lang/Block;");

    for(int i = 0; i < arity; i++)
    {
      builder.append("Lmatula/lang/Node;");
    }
    
    for(int i = 0; i < blockArity; i++)
    {
      builder.append("Lmatula/lang/Block;");
    }
    
    builder.append(")").append(returnTypeDescriptor);
    return builder.toString();
  } 

  public static String methodDescriptor(int arity, int blockArity)
  {
    return methodDescriptor(arity, blockArity, "Lmatula/lang/Node;", false);
  }
}

package org.matula.compile;

public interface Constants
{
  public static final char MODULE_NAME_SEPARATOR = '.';
  public static final char INTERNAL_MODULE_NAME_SEPARATOR = '/';

  public static final char RULE_NAME_SEPARATOR = '-';
  public static final char INTERNAL_RULE_NAME_SEPARATOR = '_';

  public static final String NODE_INTERNAL_NAME = "matula/lang/Node";
  public static final String NODE_TYPE_DESCRIPTOR = String.format("L%s;", NODE_INTERNAL_NAME);

  public static final String BLOCK_INTERNAL_NAME = "matula/lang/Block";
  public static final String BLOCK_TYPE_DESCRIPTOR = String.format("L%s;", BLOCK_INTERNAL_NAME);

  public static final String CALLABLE_INTERNAL_NAME = "java/util/concurrent/Callable";
  public static final String CALLABLE_TYPE_DESCRIPTOR = String.format("L%s;", CALLABLE_INTERNAL_NAME);

  public static final String RUNTIME_INTERNAL_NAME = "matula/runtime/Runtime";

  public static final String DEFAULT_EXECUTOR_SERVICE_CONSTANT_NAME = "DEFAULT_EXECUTOR_SERVICE";
}
